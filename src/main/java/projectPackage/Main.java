package projectPackage;

import projectPackage.controller.Controller;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.ModelClone;
import projectPackage.view.View;


public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {

        TurnMachine model = new TurnMachine();
        ModelClone modelClone = new ModelClone(model.getGameBoard());
        View view = new View(System.in, System.out, modelClone);
        Controller controller = new Controller(model, view);
        
        model.registerObserver(view);
        view.registerObserver(controller);
        
        view.init();
    }
}
