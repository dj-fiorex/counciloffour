package projectPackage;

import projectPackage.controller.exceptions.ActionNotAllowedException;

public interface Observer {
	
	public <C> void update(C change) throws CloneNotSupportedException;

}
