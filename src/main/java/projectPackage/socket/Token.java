package projectPackage.socket;

/**
 * Created by carme on 17/06/2016.
 */

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A unique token used to identify a specific client, it can contain additional
 * information but only the uuid is used for identification.
 */
public class Token implements Serializable {

    private final UUID uuid;
    // Player number is optional
    private String playerName;

    /**
     * Create a new Token.
     *
     * @param uuid an unique identifier in String format
     */
    public Token(UUID uuid) {

        this.uuid = uuid;

    }

    /**
     * Get the unique identifier of the client.
     *
     * @return the unique identifier of the client
     */
    public UUID getUuid() {

        return uuid;

    }

    /**
     * Get the player number in a game.
     *
     * @return the player number in a game
     */
    public String getPlayerName() {

        return playerName;

    }

    /**
     * Set the player number in a game.
     *
     * @param playerName the player number in a game
     */
    public void setPlayerName(String playerName) {

        this.playerName = playerName;

    }

    @Override
    public boolean equals(Object o) {

        // NOTE: Only uuid is used!

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return Objects.equals(uuid, token.uuid);

    }

    @Override
    public int hashCode() {

        // NOTE: Only uuid is used!
        return Objects.hashCode(uuid);

    }

}

