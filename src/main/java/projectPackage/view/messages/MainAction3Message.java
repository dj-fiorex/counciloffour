package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class MainAction3Message extends ViewControllerMessage {
	
	private int whichPrivatePermitCard;
	private char whichCharacter;

	
	public MainAction3Message(int whichPrivatePermitCard, char whichCharacter) {
		super();
		this.whichPrivatePermitCard = whichPrivatePermitCard;
		this.whichCharacter = whichCharacter;
	}


	public int getWhichPrivatePermitCard() {
		return whichPrivatePermitCard;
	}


	public char getWhichCharacter() {
		return whichCharacter;
	}


	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
