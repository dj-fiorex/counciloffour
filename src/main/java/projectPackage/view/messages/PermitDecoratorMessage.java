package projectPackage.view.messages;

/**
 * Created by Med on 16/06/2016.
 */
public class PermitDecoratorMessage extends ModelViewMessage {

    private int region;

    private int choice;

    public PermitDecoratorMessage() {
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public int getChoice() {
        return choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }

}
