package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class QuickAction3Message extends ViewControllerMessage {

	private int whichRegion;
	private int whichCouncillor;

	public QuickAction3Message(int whichRegion, int whichCouncillor) {
		this.whichRegion = whichRegion;
		this.whichCouncillor = whichCouncillor;
	}

	public int getWhichRegion() {
		return whichRegion;
	}

	public void setWhichRegion(int whichRegion) {
		this.whichRegion = whichRegion;
	}

	public int getWhichCouncillor() {
		return whichCouncillor;
	}

	public void setWhichCouncillor(int whichCouncillor) {
		this.whichCouncillor = whichCouncillor;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
