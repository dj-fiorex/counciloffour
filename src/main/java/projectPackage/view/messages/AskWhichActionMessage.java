package projectPackage.view.messages;


public class AskWhichActionMessage extends ModelViewMessage {
	
	private int choiceAction;

	public AskWhichActionMessage(int choiceAction) {
		this.choiceAction = choiceAction;
	}

	public int getChoiceAction() {
		return choiceAction;
	}

	

}
