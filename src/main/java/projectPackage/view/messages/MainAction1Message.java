package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class MainAction1Message extends ViewControllerMessage {
	
	private int whichCouncillor;
	private int whichCouncil;

	public MainAction1Message(int whichCouncillor, int whichCouncil) {
		super();
		this.whichCouncillor = whichCouncillor;
		this.whichCouncil = whichCouncil;
	}

	public int getWhichCouncillor() {
		return whichCouncillor;
	}

	public int getWhichCouncil() {
		return whichCouncil;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
