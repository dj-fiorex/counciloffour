package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public abstract class ViewControllerMessage extends Message {

	 public abstract Action createAction(ActionFactoryVisitorImpl visitor);

}
