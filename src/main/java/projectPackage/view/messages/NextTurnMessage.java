package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.controller.action.NextTurnAction;
import projectPackage.model.turn.TurnMachine;

public class NextTurnMessage extends ViewControllerMessage {

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		// TODO Auto-generated method stub
		return visitor.visit(this);
	}

}
