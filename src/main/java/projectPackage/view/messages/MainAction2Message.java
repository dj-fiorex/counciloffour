package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class MainAction2Message extends ViewControllerMessage {
	
	private int whichCouncilRegion;
	private int whichPermitCard;

	public MainAction2Message(int whichCouncilRegion, int whichPermitCard) {
		super();
		this.whichCouncilRegion = whichCouncilRegion;
		this.whichPermitCard = whichPermitCard;
	}

	
	
	public int getWhichCouncilRegion() {
		return whichCouncilRegion;
	}



	public int getWhichPermitCard() {
		return whichPermitCard;
	}



	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
