package projectPackage.view.messages;

/**
 * Created by Med on 16/06/2016.
 */
public class WhichOfYourCitiesMessage extends ModelViewMessage {

    public WhichOfYourCitiesMessage(boolean isDoubleDec) {
        this.isDoubleDec = isDoubleDec;
    }

    private char choice;

    private char secondChoice;

    private boolean isDoubleDec;

    public char getChoice() {
        return choice;
    }

    public void setChoice(char choice) {
        this.choice = choice;
    }

    public boolean isDoubleDec() {
        return isDoubleDec;
    }

    public void setDoubleDec(boolean aDouble) {
        isDoubleDec = aDouble;
    }

    public char getSecondChoice() {
        return secondChoice;
    }

    public void setSecondChoice(char secondChoice) {
        this.secondChoice = secondChoice;
    }
}
