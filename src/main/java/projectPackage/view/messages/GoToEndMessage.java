package projectPackage.view.messages;

import projectPackage.model.Player;

public class GoToEndMessage extends ModelViewMessage {
	
	private Player player;

	public GoToEndMessage(Player player) {
		super();
		this.player = player;
	}

	
	
	public Player getPlayer() {
		return player;
	}

}
