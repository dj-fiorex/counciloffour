package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class QuickAction2Message extends ViewControllerMessage {

	private int whichRegion;

	public QuickAction2Message(int whichRegion) {
		this.whichRegion = whichRegion;
	}

	public int getWhichRegion() {
		return whichRegion;
	}

	public void setWhichRegion(int whichRegion) {
		this.whichRegion = whichRegion;
	}

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
