package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class EndActionMessage extends ViewControllerMessage {

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
