package projectPackage.view.messages;


import java.util.*;
import projectPackage.controller.action.*;
import projectPackage.controller.action.ActionFactoryVisitorImpl;
import projectPackage.model.*;

public class InitMessage extends ViewControllerMessage {

	private LinkedList<Player> players;
	
	public InitMessage(){
		players = new LinkedList<>();
	}
	

	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}
	
	public void addPlayer(Player p){
		players.add(p);
	}

	public LinkedList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(LinkedList<Player> players) {
		this.players = players;
	}

}
