package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class MainAction4Message extends ViewControllerMessage {
	
	private char whichCity;

	public MainAction4Message(char whichCity) {
		this.whichCity = whichCity;
	}
	
	

	public char getWhichCity() {
		return whichCity;
	}



	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
