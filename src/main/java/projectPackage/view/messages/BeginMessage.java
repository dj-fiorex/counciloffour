package projectPackage.view.messages;

import projectPackage.controller.action.Action;
import projectPackage.controller.action.ActionFactoryVisitorImpl;

public class BeginMessage extends ViewControllerMessage {
	
	private int choiceAction;

	public BeginMessage(int choiceAction) {
		super();
		this.choiceAction = choiceAction;
	}
	

	public int getChoiceAction() {
		return choiceAction;
	}


	@Override
	public Action createAction(ActionFactoryVisitorImpl visitor) {
		return visitor.visit(this);
	}

}
