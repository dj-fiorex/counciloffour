package projectPackage.view.messages;

/**
 * Created by Med on 16/06/2016.
 */
public class HandPermitDecoratorMessage extends ModelViewMessage {

    public HandPermitDecoratorMessage() {
    }

    private int choice;

    public int getChoice() {
        return choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }
}
