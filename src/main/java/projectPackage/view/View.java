package projectPackage.view;


import java.awt.Color;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.*;
import projectPackage.controller.exceptions.*;

import projectPackage.model.City;
import projectPackage.model.Player;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.utility.ColorParser;
import projectPackage.view.messages.*;

public class View extends projectPackage.Observable implements projectPackage.Observer  {
	
	
    private Scanner scanner;
    private PrintStream output;
    private ModelClone modelClone;
    
    private boolean [] mainDone;
    private boolean [] quickDone;

    public View(InputStream input, OutputStream output, ModelClone modelClone) {
        this.scanner = new Scanner(input);
        this.output = new PrintStream(output);
        this.modelClone = modelClone;
        mainDone = new boolean [4];
        quickDone = new boolean [4];
    }

	public ModelClone getModelClone() {
		return modelClone;
	}

	public void setModelClone(ModelClone modelClone) {
		this.modelClone = modelClone;
	}

	public void init() throws CloneNotSupportedException {
    	//Timer t = new Timer();
    	InitMessage im = new InitMessage();//aggiungere la mappa
    	LinkedList<Color> colors = new LinkedList<Color>();
    	colors.add(Color.BLACK);
    	colors.add(Color.BLUE);
    	colors.add(Color.GRAY);
    	colors.add(Color.GREEN);
    	colors.add(Color.YELLOW);
    	colors.add(Color.ORANGE);
    	int i=0;
    	loop:
        while(true){
        	if(i == 5) break;
        	System.out.println("What's your name? insert 0 to finish");
        	String s = scanner.nextLine();
        	if(s.length() == 0){
        		System.out.println("You didn't write nothing retype a name!");
        		continue loop;
        	}

        	if(s.charAt(0) == '0'){
        		if(im.getPlayers().size() < 2){
        			System.out.println("Must be exists minimum two players!");
        			continue loop;
        		}
        		else break;
        	}
        	//Player p = new Player(s,colors.get(i));
			Player p = new Player(s,Color.black);
        	im.addPlayer(p);
        	i++;
        }
        this.notifyObservers(im);
    }


	public int choiceWhichMainAction(){
		output.println("Which main action do you want to do?\n" +
				"1) Press 1 for: Elect a councillor\n" +
				"2) Press 2 for: Acquire a business permit tile\n" +
				"3) Press 3 for: Build an emporium using permit tile\n" +
				"4) Press 4 for: Build an emporium with the help of the King");
		int i = scanner.nextInt();
		if (i<1 || i>4) throw new IllegalArgumentException();
		return i;
	}
	
	public int choiceWhichQuickAction(){
		output.println("Which quick action do you want to choose?\n" +
				"1) Press 1 for: Engage an assistant\n" +
				"2) Press 2 for: Change permit tile\n" +
				"3) Press 3 for: Send an assistant to elect a councillor\n" +
				"4) Press 4 for: Perform an additional main action");
		int i = scanner.nextInt();
		if (i<1 || i>4) throw new IllegalArgumentException();
		return i;
	}
	
	public int choiceMainOrQuick(){
		output.println(modelClone.currentPlayerSituation());  //stampo la situazione attuale dell'utente
		output.println("Do you want to do first the MAIN action or the QUICK action?\n" + 
				   "1) Press 1 for: Main action\n" + 
				   "2) Press 2 for: Quick action");
		int i = scanner.nextInt();
		if (i<1 || i>2) throw new IllegalArgumentException();
		return i;
	}

	public int chooseWhichCouncil() {
		output.println(modelClone.getCouncils());
		output.println("Choose a council:\n" +
				"Type 1 for COAST\n" +
				"Type 2 for HILLS\n" +
				"Type 3 for MOUNTAINS\n" +
				"Type 4 for KING Council");

		int i = scanner.nextInt();
		if ( i < 1  ||  i > 4 ) throw new IllegalArgumentException();
		return i;
	}

	public int chooseWhichRegionCouncil() {
		output.println(modelClone.getRegionCouncils());
		output.println("Choose a council:\n" +
				"Type 1 for COAST\n" +
				"Type 2 for HILLS\n" +
				"Type 3 for MOUNTAINS\n");
		int i = scanner.nextInt();
		if ( i < 1  ||  i > 3 ) throw new IllegalArgumentException();
		return i;
	}

	public int chooseWhichRegion() {
		output.println("Choose a council:\n" +
				"Type 1 for COAST\n" +
				"Type 2 for HILLS\n" +
				"Type 3 for MOUNTAINS\n");
		int i = scanner.nextInt();
		if ( i < 1  ||  i > 3 ) throw new IllegalArgumentException();
		return i;
	}

	public int chooseCouncillorFromReserve() {
		System.out.println("Choose one of the available councillors at the side of the board\n");
		System.out.println(modelClone.getReserveOfCouncillors());
		int i = scanner.nextInt();
		if(i < 1 || i > 8) throw new IllegalArgumentException();
		return i;
	}

	public void exceptions(RuntimeException e, int mainOrQuick,int whichOfFour){
		if(e instanceof IllegalArgumentException){
			System.out.println("Input error! Retype!");
			return;
		}
		if(mainOrQuick == 1){
			switch(whichOfFour){
				case 1:
					System.out.println("Can't be exception here!");
					break;
				case 2:
					if(e instanceof NotEnoughPoliticCards){
						System.out.println("You don't have enough Politic Cards for corrupt this council! Change Council!");
					}
					if(e instanceof MissingPermitCard){
						System.out.println("Permit Card not found! Choose an other Permit Card!");
					}
					break;
				case 3:
					if(e instanceof AlreadyBuiltException){
						System.out.println("You have already built in this city! Choose an other city!");
					}
					if(e instanceof NotEnoughAssistants){
						System.out.println("You can't choose this city because you don't have enough assistants! Choose an other city!");
					}
					break;
				default:
					if(e instanceof NotEnoughPoliticCards){
						System.out.println("You can't do this action because you don't have enough Politic Cards!");
					}
					if(e instanceof NotEnoughMoneyForMove){
						System.out.println("You don't have enough money for move the king in this city! Change city!");
					}
					if(e instanceof AlreadyBuiltException){
						System.out.println("You have already built in this city! Choose an other city!");
					}
					if(e instanceof NotEnoughAssistants){
						System.out.println("You can't choose this city because you don't have enough assistants! Choose an other city!");
					}
					break;
			}
		}
		else{
			switch(whichOfFour){
				case 1:
					if(e instanceof NotEnoughMoney){
						System.out.println("You can't do this action because you don't have enough money!");
						quickDone[0] = true;
					}
					quickDone[0] = true;
					break;
				case 2:
					if(e instanceof NotEnoughAssistants){
						System.out.println("You can't do this action because you don't have enough assistants!");
					}
					quickDone[1] = true;
					break;
				case 3:
					if(e instanceof NotEnoughAssistants){
						System.out.println("You can't do this action because you don't have enough assistants!");
					}
					quickDone[2] = true;
					break;
				default:
					if(e instanceof NotEnoughAssistants){
						System.out.println("You can't do this action because you don't have enough assistants!");
					}
					quickDone[3] = true;
					break;
			}
		}
	}
	
	/*Those two following methods are necessary for calculate
	 * which main or quick actions have already done the current player 
	 * for every his turn.
	 */
	
	public boolean allQuickActionTried(){
		boolean flag = true;
		for(int i=0;i<quickDone.length;i++){
			if(!quickDone[i]) flag = false;
		}
		return flag; //ora e' giusto cosi' med perche' devo trollarle tutte!!!
	}
	
	public boolean allMainActionTried(){
		boolean flag = true;
		for(int i=0;i<mainDone.length;i++){
			if(!mainDone[i]) flag = false;
		}
		return flag; //ora e' giusto cosi' med perche' devo trollarle tutte!!!
	}
	

	@Override
	public <C> void update(C change) throws CloneNotSupportedException{
		if(change instanceof AskActionMessage){
			int choiceAction = choiceMainOrQuick();
	        notifyObservers(new BeginMessage(choiceAction));
		}
		if(change instanceof AskWhichActionMessage){
			while(true){
				boolean flag = false;
				int choiceAction = ((AskWhichActionMessage)change).getChoiceAction();
				int choiceWhichAction;
				if (choiceAction == 1) choiceWhichAction = choiceWhichMainAction();
				else choiceWhichAction = choiceWhichQuickAction();
				try{
					Message m = generateMessage(choiceAction,choiceWhichAction);
					notifyObservers(m);
				}catch(RuntimeException e){
					exceptions(e,choiceAction,choiceWhichAction);
				}finally{
					if(!allQuickActionTried()) flag = true;
				}
				if(!flag) break;
			}
			if(allQuickActionTried()){
				for(int i=0;i<quickDone.length;i++){
					quickDone[i] = false;
				}
				System.out.println("You can't do any quick action!");
				notifyObservers(new EndActionMessage());
			}
		}
		if (change instanceof AskQuickActionMessage){
			Message m;
			System.out.println("Do you want to do the Quick Action?\n S/N");
			char c = scanner.next().trim().toUpperCase().charAt(0);
			if(c == 'S'){
				while(true){
					boolean flag = false;
					int choiceActionNumber = choiceWhichQuickAction();
					try{
						m = generateMessage(2,choiceActionNumber);
						notifyObservers(m);
					}catch(RuntimeException e){
						exceptions(e,2,choiceActionNumber);
					}finally{
						if(!allQuickActionTried()) flag = true;
					}
					if(!flag) break;
				}
			}
			else if(c == 'N'){
				m = new EndActionMessage();
				notifyObservers(m);
			}
			else throw new IllegalArgumentException();
			if(allQuickActionTried()){
				for(int i=0;i<quickDone.length;i++){
					quickDone[i] = false;
				}
				System.out.println("You can't do any quick action!");
				notifyObservers(new EndActionMessage());
			}
		}
		if(change instanceof AskMainActionMessage){
			while(true){
				boolean flag = false;
				int choiceActionNumber = choiceWhichMainAction();
				try{
					Message m = generateMessage(1,choiceActionNumber);
					notifyObservers(m);
				}catch(RuntimeException e){
					exceptions(e,1,choiceActionNumber);
				}finally{
					flag = true;
				}
				if(!flag) break;
			}
		}
		if(change instanceof GoToEndMessage){
			String name = ((GoToEndMessage)change).getPlayer().getName(); 
			System.out.println(name+" you have finished your turn!\n");
			notifyObservers(new NextTurnMessage());
		}
		if(change instanceof BuyMessage){
			output.println("Benvenuto nella fase di acquisto del market cazzo!");

		}
		if(change instanceof SellMessage){
			StringBuilder sb = new StringBuilder(300);
			sb.append("Welcome to sell market-state cazzo!\nHere you can sell your stuff like Politic Card, Permit Card and Assistant too\nThis is your situation:\n");
			sb.append("Politic Card:");
			StringBuilder sb2 = new StringBuilder(400);
			sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			for(PoliticCard p : modelClone.getHandPoliticDeck()){
				sb2.append("++   " + ColorParser.getStringFromColor(p.getColorCard())+ "    ");
			}
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
			sb.append(sb2);
			sb2.delete(0, sb2.capacity());
			sb.append("Permit Card:");
			sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			for(PermitCard p : modelClone.getHandPermitDeck()){
				for(char c : p.getCities()){
					sb2.append("++     "+c+"     ++");
				}
			}
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
			sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
			sb.append(sb2);
			sb2.delete(0, sb2.capacity());
			sb.append("What do you want to sell?");
			output.println(sb);
		}

		if (change instanceof WhichOfYourCitiesMessage) {
			LinkedList<City> list = modelClone.getCitiesOfPlayerExceptNobilityDec();
			if (list.isEmpty()) throw new ActionNotAllowedException();
			System.out.print(list);
			LinkedList<Character> shortList = new LinkedList<>();
			for (City c : list) {
				shortList.add(c.getShortName());
			}
			System.out.println("Choose one of the cities you have built on" +
					"to activate its Coin." +
					"You can't activate a coin which bonuses includes moving on the Nobility Track." +
					"Type the shortname of the city you have chosen");
			char c = scanner.nextLine().trim().toUpperCase().charAt(0);
			if (!shortList.contains(c)) throw new IllegalArgumentException();
			((WhichOfYourCitiesMessage) change).setChoice(c);
			if (((WhichOfYourCitiesMessage) change).isDoubleDec()) {
				System.out.println("Choose another one of the cities you have built on" +
						"to activate its Coin." +
						"You can't activate a coin which bonuses includes moving on the Nobility Track." +
						"Type the shortname of the city you have chosen");
				char ch = scanner.nextLine().trim().toUpperCase().charAt(0);
				if (!shortList.contains(ch)) throw new IllegalArgumentException();
				((WhichOfYourCitiesMessage) change).setSecondChoice(ch);
			}
		}

		if (change instanceof HandPermitDecoratorMessage) {
			LinkedList<PermitCard> list = modelClone.getHandPermitDeck();
			LinkedList<PermitCard> graveList = modelClone.getGraveDeck();
			System.out.println("Choose one the permit card you have in your hand or in your grave deck to activate its bonuses");
			list.addAll(graveList);
			System.out.print(list);
			int choice = scanner.nextInt();
			if (choice>list.size()) throw new IllegalArgumentException();
            ((HandPermitDecoratorMessage) change).setChoice(choice);
		}

		if (change instanceof PermitDecoratorMessage) {
			System.out.println("You can take a face-up permit card");
			System.out.print(modelClone.getFirstRegionPermitCards(1));
			System.out.print(modelClone.getSecondRegionPermitCards(1));
			System.out.print(modelClone.getFirstRegionPermitCards(2));
			System.out.print(modelClone.getSecondRegionPermitCards(2));
			System.out.print(modelClone.getFirstRegionPermitCards(3));
			System.out.print(modelClone.getSecondRegionPermitCards(3));
			System.out.println("Choose one region:" +
					"Type 1 for COAST\n" +
					"Type 2 for MOUNTAINS\n" +
					"Type 3 for HILLS\n");
			int region = scanner.nextInt();
			if (region<1 || region>3) throw new IllegalArgumentException();
			System.out.println("Choose the permit card:\n" +
					"Type 1 for the First one\n" +
					"Type 2 for the Second one\n");
			int choice = scanner.nextInt();
			if (choice<1 || choice>2)throw new IllegalArgumentException();
            ((PermitDecoratorMessage) change).setRegion(region);
			((PermitDecoratorMessage) change).setChoice(choice);
		}
	}
	
	/*This method is necessary to generate the correct message in 
	 *corrispondence of the users' input choice
	 */

	private Message generateMessage(int choiceAction,int choiceActionNumber) throws CloneNotSupportedException {
        if(choiceAction==1){
        	switch(choiceActionNumber){
        		case 1: 
        			int whichCouncillor,whichCouncil;
        			whichCouncillor = chooseCouncillorFromReserve();
					whichCouncil = chooseWhichCouncil();
        			return new MainAction1Message(whichCouncillor,whichCouncil);
        		case 2:
        			//controllo se le carte per soddisfare il consiglio possedute sono maggiori di 1
        			int whichCouncilRegion,whichPermitCard;
        			whichCouncilRegion = chooseWhichRegionCouncil();
        			System.out.println("Insert 1 for following Permit Card: "+modelClone.getFirstRegionPermitCards(whichCouncilRegion)+"\n");
        			System.out.println("Insert 2 for following Permit Card: "+modelClone.getSecondRegionPermitCards(whichCouncilRegion)+"\n");
        			System.out.println("Choose one of the permit cards in front of the Region that you just chose\n");
        			whichPermitCard = scanner.nextInt();
        			if(whichPermitCard < 1 || whichPermitCard > 2){
        				throw new IllegalArgumentException();
        			}
					System.out.print("muzzo2");
        			return new MainAction2Message(whichCouncilRegion,whichPermitCard);
        		case 3:
        			//controllo se tessera permesso e' disponibile e se possiede il carattere scelto
        			int whichPrivatePermitCard;
        			char whichCharacter;
        			LinkedList<PermitCard> privatePermitCards = modelClone.getPrivatePermitCards();
        			System.out.println(privatePermitCards);
        			System.out.println("Choose one of the Permit Cards face up in front of you\n");
        			whichPrivatePermitCard = scanner.nextInt();
        			if(whichPrivatePermitCard < 1 || whichPrivatePermitCard > privatePermitCards.size()) throw new IllegalArgumentException();
        			char [] cities =  privatePermitCards.get(whichPrivatePermitCard-1).getCities();
        			System.out.println("The cities on the Permit Card which you just chose:\n");
        			System.out.println("[");
        			for(int i=0;i<cities.length;i++){
        				System.out.println(cities[i]+", ");
        			}
        			System.out.println("]\n");
        			System.out.println("Choose one of the cities which are indicated on the Permit Card\n");
        			whichCharacter = scanner.nextLine().trim().toUpperCase().charAt(0);
        			//controllare se la lettera e' presente altrimenti tornare errore sintattico(serve accesso a tessera permesso nel model)
        			boolean flag = false;
        			for(int i=0;i<cities.length;i++){
        				if(cities[i]==whichCharacter) flag = true;
        			}
        			if(!flag) throw new IllegalArgumentException();
        			return new MainAction3Message(whichPrivatePermitCard, whichCharacter);
        		default:
        			//controllo se le carte per soddisfare il consiglio possedue sono maggiori di 1
        			char whichCity;
        			System.out.println("You are using Council of the King\n");
        			LinkedList<City> l = modelClone.getAllCities();
        			System.out.println(l);
        			System.out.println("Choose the city where you want to build an emporium (using short name)\n");
        			whichCity = scanner.nextLine().trim().charAt(0); // se scrive ciao lo prende come giusto (correggiere)
        			if(!modelClone.containsCity(whichCity)) throw new IllegalArgumentException();
        			return new MainAction4Message(whichCity);
        	}
        }
        else{
        	switch(choiceActionNumber){
        		case 1:
        			//controllo se il giocatore ha almeno 3 monete
        			return new QuickAction1Message();
        		case 2:
        			//controllo se il giocatore ha almeno un aiutante
					int whichRegion;
					System.out.println("Which region do you want to change permit tiles?\n" +
							"Type 1 for COAST, 2 for HILLS, 3 for MOUNTAINS");
					whichRegion = scanner.nextInt();
					if (whichRegion<1 || whichRegion>3) throw new IllegalArgumentException();
        			return new QuickAction2Message(whichRegion);
        		case 3:
        			//controllo se il giocatore ha almeno un aiutante
					int council = chooseWhichCouncil();
					int councillor = chooseCouncillorFromReserve();

        			return new QuickAction3Message(council, councillor);
        		default:
        			//controllo se il giocatoe ha almeno 3 aiutanti
        			return new QuickAction4Message();
        	}
        }	
	}
}
