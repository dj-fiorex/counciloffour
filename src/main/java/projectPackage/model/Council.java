package projectPackage.model;

import java.util.*;


public class Council {

    private LinkedList<Councillor> councillors;
    private TypeCouncil typeOfCouncil;

    public Council(LinkedList<Councillor> councillors, TypeCouncil typeOfCouncil) {
		super();
		this.councillors = councillors;
		this.typeOfCouncil = typeOfCouncil;
	}

    public void addFirst(Councillor c){
    	councillors.addFirst(c);
    }
    
    public void addLast(Councillor c){
    	councillors.addLast(c);
    }
    
    public TypeCouncil getTypeOfCouncil() {
		return typeOfCouncil;
	}
    
    public Councillor getFirst(){
    	return councillors.getFirst();
    }
    
    public Councillor getLast(){
    	return councillors.getLast();
    }
    
    public Councillor removeFirst(){
    	return councillors.removeFirst();
    }
    
    public Councillor removeLast(){
    	return councillors.removeLast();
    }
    
	public void setCouncillors(LinkedList<Councillor> councillors) {
		this.councillors = councillors;
	}

	public LinkedList<Councillor> getCouncillors() {
        return councillors;
    }

    public Councillor corrupt(Councillor councillor) {
        Councillor old = councillors.removeLast();
        councillors.addFirst(councillor);
        return old;
    }
    
    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("******************************************\n");
    	sb.append("*            "+typeOfCouncil+"           *\n");
    	sb.append("******************************************\n");
    	for(Councillor c : councillors){
    		sb.append(c);
    	}
    	return sb.toString();
    }

}
