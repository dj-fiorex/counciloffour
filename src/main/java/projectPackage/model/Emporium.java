package projectPackage.model;

import java.awt.Color;

public class Emporium {
	
	private final Color color;
	
	public Emporium(Color c){
		this.color = c;
	}

	public Color getColor() {
		return color;
	}

	public boolean equals(Object o) {
        if(!(o instanceof Emporium)) return false;
        if(o==this) return true;
        Emporium e = (Emporium)o;
		if(!e.color.equals(this.color)) return false;
		return true;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder(200);
		sb.append("*******************************\n");
		sb.append("*                             *\n");
		sb.append("*                             *\n");
		if(color != null) sb.append("*          "+color+"          *\n");
		else sb.append("*          Empty         *\n");
		sb.append("*                             *\n");
		sb.append("*                             *\n");
		sb.append("*******************************\n");
		return sb.toString();
	}
}