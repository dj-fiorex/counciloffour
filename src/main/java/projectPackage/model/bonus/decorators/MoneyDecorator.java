package projectPackage.model.bonus.decorators;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.track.square.MoneySquare;
import projectPackage.model.track.square.Square;

/**
 * Created by Med on 19/05/2016.
 */


public class MoneyDecorator extends BonusDecorator  {

    private int number;

    public MoneyDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    public void activateBonusDecorator (){
        getGameBoard().moveOnMoneyTrack(number);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException{
        super.activateBonus();
    }


}
