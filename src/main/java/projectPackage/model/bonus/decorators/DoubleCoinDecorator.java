package projectPackage.model.bonus.decorators;


import projectPackage.model.bonus.Bonus;
import projectPackage.view.messages.WhichOfYourCitiesMessage;

/**
 * Created by Med on 23/05/2016.
 */
public class DoubleCoinDecorator extends BonusDecorator {

    public DoubleCoinDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        WhichOfYourCitiesMessage m = new WhichOfYourCitiesMessage(true);
        getTurnMachine().notifyObservers(m);
        getGameBoard().getCity(m.getChoice()).getCoin().activateBonus();
        getGameBoard().getCity(m.getSecondChoice()).getCoin().activateBonus();
    }
}
