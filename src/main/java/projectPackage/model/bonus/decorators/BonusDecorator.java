package projectPackage.model.bonus.decorators;

import projectPackage.model.GameBoard;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.turn.TurnMachine;

/**
 * Created by Med on 19/05/2016.
 */

abstract class BonusDecorator implements Bonus {

    protected Bonus tempBonus;

    @Override
    public TurnMachine getTurnMachine() {
        return tempBonus.getTurnMachine();
    }

    public GameBoard getGameBoard() {
        return tempBonus.getGameBoard();
    }

    public BonusDecorator(Bonus newBonus) {
        this.tempBonus = newBonus;
    }

    @Override
    public  void activateBonus() throws CloneNotSupportedException {
        tempBonus.activateBonus();
        activateBonusDecorator();
    }

    protected abstract void activateBonusDecorator() throws CloneNotSupportedException;


    public boolean hasNobilityDecorator() {
        return tempBonus.hasNobilityDecorator();
    }

    @Override
    public void setNobilityDecorator(boolean value) {
        tempBonus.setNobilityDecorator(value);
    }
}
