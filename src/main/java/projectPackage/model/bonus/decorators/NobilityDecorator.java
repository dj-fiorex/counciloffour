package projectPackage.model.bonus.decorators;

import projectPackage.model.bonus.Bonus;

/**
 * Created by Med on 21/05/2016.
 */
public class NobilityDecorator extends BonusDecorator {

    private int number;

    public NobilityDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        getGameBoard().moveOnNobilityTrack(number);
        tempBonus.setNobilityDecorator(true);
    }
}
