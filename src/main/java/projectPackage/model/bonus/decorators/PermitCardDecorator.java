package projectPackage.model.bonus.decorators;


import projectPackage.model.Region;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.view.messages.PermitDecoratorMessage;

public class PermitCardDecorator extends BonusDecorator {

    public PermitCardDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException{
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        PermitDecoratorMessage msg = new PermitDecoratorMessage();
        getTurnMachine().notifyObservers(msg);
        Region region = getGameBoard().getRegion(msg.getRegion());
        PermitCard pc = new PermitCard();
        if (msg.getChoice()==1) {
            pc = region.getFirstPermitCard();
            getGameBoard().getCurrPlayer().addPermitCard((PermitCard) pc.clone());
            region.setFirstPermitCard(region.pickACard());
        }
        else if (msg.getChoice()==2) {
            pc = region.getSecondPermitCard();
            getGameBoard().getCurrPlayer().addPermitCard((PermitCard)pc.clone());
            region.setSecondPermitCard(region.pickACard());
        }

    }
}
