package projectPackage.model.bonus.decorators;

import projectPackage.model.bonus.Bonus;
import projectPackage.view.messages.WhichOfYourCitiesMessage;

/**
 * Created by Med on 23/05/2016.
 */
public class CoinDecorator extends BonusDecorator {


    public CoinDecorator(Bonus newBonus) {
        super(newBonus);
    }


    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        WhichOfYourCitiesMessage m = new WhichOfYourCitiesMessage(false);
        getTurnMachine().notifyObservers(m);
        char c = m.getChoice();
        getGameBoard().getCity(c).getCoin().activateBonus();
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        super.activateBonus();
    }


}
