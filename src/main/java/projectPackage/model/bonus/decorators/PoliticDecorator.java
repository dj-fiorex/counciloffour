package projectPackage.model.bonus.decorators;


import projectPackage.model.bonus.Bonus;

/**
 * Created by Med on 23/05/2016.
 */
public class PoliticDecorator extends BonusDecorator {

    private int number;

    public PoliticDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException{
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() {
        for (int i=0; i<number; i++) {
            getGameBoard().drawCard();
        }
    }
}
