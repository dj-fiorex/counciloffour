package projectPackage.model.bonus.decorators;


import projectPackage.model.bonus.Bonus;
import projectPackage.model.track.square.Square;
import projectPackage.model.track.square.VictorySquare;

/**
 * Created by Med on 21/05/2016.
 */
public class VictoryDecorator extends BonusDecorator {

    private int number;

    public VictoryDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException{
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() {
        getGameBoard().moveOnVictoryTrack(number);
    }
}
