package projectPackage.model.bonus.decorators;

import projectPackage.model.*;
import projectPackage.model.bonus.Bonus;
import projectPackage.view.messages.AskMainActionMessage;
import projectPackage.view.messages.Message;

/**
 * Created by Med on 23/05/2016.
 */
public class ActionDecorator extends BonusDecorator {

    public ActionDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException {
        Message m = new AskMainActionMessage();
        getTurnMachine().notifyObservers(m);
    }
}
