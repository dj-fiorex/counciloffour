package projectPackage.model.bonus.decorators;


import projectPackage.controller.exceptions.ActionNotAllowedException;
import projectPackage.model.bonus.Bonus;
import projectPackage.view.messages.HandPermitDecoratorMessage;

/**
 * Created by Med on 23/05/2016.
 */
public class HandPermitCardDecorator extends BonusDecorator{

    public HandPermitCardDecorator(Bonus newBonus) {
        super(newBonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException{
        super.activateBonus();
    }

    @Override
    protected void activateBonusDecorator() throws CloneNotSupportedException{
        HandPermitDecoratorMessage m = new HandPermitDecoratorMessage();
        getTurnMachine().notifyObservers(m);
        int i = getGameBoard().getCurrPlayer().getPermitDeck().size();
        if (m.getChoice()<=i) {
            getGameBoard().getCurrPlayer().getPermitDeck().getCard(m.getChoice()).activateBonus();
        }
        else {
            int n = m.getChoice()-i;
            getGameBoard().getCurrPlayer().getGraveDeck().getCard(n).activateBonus();
        }

    }
}
