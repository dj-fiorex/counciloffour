package projectPackage.model.bonus.decorators;


import projectPackage.model.bonus.Bonus;

/**
 * Created by Med on 20/05/2016.
 */

public class AssistantDecorator extends BonusDecorator {

    private int number;

    public AssistantDecorator(Bonus newBonus, int number) {
        super(newBonus);
        this.number = number;
    }

    @Override
    protected void activateBonusDecorator() {
        getGameBoard().getCurrPlayer().acquireAssistant(number);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        super.activateBonus();
    }


}
