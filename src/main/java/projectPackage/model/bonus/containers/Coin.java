package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;

/**
 * Created by Med on 19/05/2016.
 */

public class Coin extends BonusContainer {

    public Coin(Bonus bonus) {
        super(bonus);
    }

    private boolean hasNobilityDecorator = false;

	@Override
	public void activateBonus() throws CloneNotSupportedException {
		System.out.print("Coin,");
		super.activateBonus();
	}

	public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("    ***********     \n");
    	sb.append("   *           *    \n");
    	sb.append("  *             *   \n");
    	sb.append(" *               *  \n");
    	sb.append("*  stampa bonus   *  \n");
    	sb.append(" *               *  \n");
     	sb.append("  *             *   \n");
     	sb.append("   *           *    \n");
     	sb.append("    ***********     \n");
     	return sb.toString();
    }

}
