package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;
import projectPackage.model.utility.ColorParser;

import java.awt.*;

/**
 * Created by Med on 21/05/2016.
 */

public class ColorCard extends BonusContainer implements Card {

    private Color color;

    public ColorCard(Bonus bonus, Color color) {
        super(bonus);
        this.color = color;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("ColorCard,");
        super.activateBonus();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("* "+ ColorParser.getStringFromColor(color)+" *\n");
    	sb.append("*       *\n");
    	sb.append("*********\n");
    	return sb.toString();
    	
    	//manca la stampa del bonus
	}


}
