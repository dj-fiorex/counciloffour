package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;

/**
 * Created by Med on 10/05/2016.
 */

public class PermitCard extends BonusContainer implements Card,Cloneable {

    private char[] cities; //RICORDA: METTERE TUTTI I CARATTERI IN MAIUSCOLO NELLA FASE DI INZIALIZZAZIONE

	public PermitCard() {
	}

	public PermitCard(Bonus bonus, char[] cities) {
        super(bonus);
        this.cities = cities;
        for(int i=0;i<this.cities.length;i++){
        	this.cities[i] = Character.toUpperCase(this.cities[i]);
        }
    }

	public char[] getCities() {
		return cities;
	}

	@Override
	public void activateBonus() throws CloneNotSupportedException {
		System.out.print("PermitCard,");
		super.activateBonus();
	}

	public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("*     [");
    	for(int i=0;i<cities.length;i++){
    		sb.append(cities[i]);
    		if(i!=cities.length-1) sb.append(",");
    	}
    	sb.append("]     *");
    	sb.append("*       *\n");
    	sb.append("*********\n");
    	return sb.toString();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new PermitCard(bonus, cities);
	}
}
