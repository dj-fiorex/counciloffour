package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;

/**
 * Created by Med on 23/05/2016.
 */

public class KingCard extends BonusContainer implements Card {

    public KingCard(Bonus bonus) {
        super(bonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("KingCard,");
        super.activateBonus();

    }
    
    public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	//stampa del bonus
    	return sb.toString();
	}


}
