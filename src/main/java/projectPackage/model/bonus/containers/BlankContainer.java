package projectPackage.model.bonus.containers;

import projectPackage.model.GameBoard;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.decorators.NobilityDecorator;
import projectPackage.model.turn.TurnMachine;

/**
 * Created by Med on 15/06/2016.
 */
public class BlankContainer implements Bonus {

    private TurnMachine turnMachine;

    private boolean nobilityDecorator;

    public BlankContainer(TurnMachine tm) {
        this.turnMachine = tm;
    }

    @Override
    public GameBoard getGameBoard() {
        return turnMachine.getGameBoard();
    }

    @Override
    public void activateBonus() {
        System.out.println("I'm activating the bonuses:");
    }

    @Override
    public TurnMachine getTurnMachine() {
        return turnMachine;
    }

    @Override
    public boolean hasNobilityDecorator() {
        return nobilityDecorator;
    }

    @Override
    public void setNobilityDecorator(boolean value) {
        this.nobilityDecorator = value;
    }
}
