package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.cards.Card;

/**
 * Created by Med on 16/05/2016.
 */
public class RegionCard extends BonusContainer implements Card {

    public RegionCard(Bonus bonus) {
        super(bonus);
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("RegionCard,");
        super.activateBonus();
    }

    public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	// stampa del bonus
    	return sb.toString();
	}

}
