package projectPackage.model.bonus.containers;

import projectPackage.model.GameBoard;
import projectPackage.model.bonus.Bonus;

/**
 * Created by Med on 21/05/2016.
 */

public abstract class BonusContainer{

    public BonusContainer() {
    }

    protected Bonus bonus;

    public BonusContainer(Bonus b) {
        this.bonus = b;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public void activateBonus() throws CloneNotSupportedException {
        this.bonus.activateBonus();
    }
}
