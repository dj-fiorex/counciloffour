package projectPackage.model.bonus.containers;

import projectPackage.model.bonus.Bonus;
import projectPackage.model.track.square.Square;

/**
 * Created by Med on 15/05/2016.
 */
public class NobilitySquare extends BonusContainer implements Square {

    private int position;

    public NobilitySquare(Bonus bonus, int position) {
        super(bonus);
        this.position = position;
    }

    @Override
    public void activateBonus() throws CloneNotSupportedException {
        System.out.print("NobilitySquare,");
        super.activateBonus();
    }

    public int getPosition(){
    	return position;
    }
    
    public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("* "+position+" *\n");
    	sb.append("*       *\n");
    	sb.append("*********\n");
    	return sb.toString();
	}
}
