package projectPackage.model.bonus;

import projectPackage.model.GameBoard;
import projectPackage.model.turn.TurnMachine;

/**
 * Created by Med on 10/05/2016.
 */
public interface Bonus {

    void activateBonus() throws CloneNotSupportedException;

    GameBoard getGameBoard();

    TurnMachine getTurnMachine();

    boolean hasNobilityDecorator();

    void setNobilityDecorator(boolean value);

}
