package projectPackage.model.track.square;

/**
 * Created by Med on 15/05/2016.
 */
public class VictorySquare implements Square {

    private int position;

	public VictorySquare(int position) {
		super();
		this.position = position;
	}

	public int getPosition() {
		return position;
	}
    
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
	public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("* "+position+" *\n");
    	sb.append("*       *\n");
    	sb.append("*********\n");
    	return sb.toString();
	}
}
