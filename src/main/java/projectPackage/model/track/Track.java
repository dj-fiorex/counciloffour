package projectPackage.model.track;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

import projectPackage.model.track.square.Square;
import projectPackage.model.track.square.VictorySquare;


/**
 * Created by Med on 15/05/2016.
 */

public class Track<C extends Square> implements Iterable<C>{

    protected C track[];
    protected int size;


	public Track(int length) {
		super();
		this.track = (C[]) new Square [length];
		size = 0;
		/*
		for(int i=0;i<length;i++){
    		this.track[i] = (C)new Object();
    		
    	}
    	*/
	}
    
    public int indexOf(C elem){
    	for(int i=0;i<size;i++){
    		if(track[i].equals(elem)) return i;
    	}
    	return -1;
    }
    
    public boolean contains(C elem){
    	return (indexOf(elem) != -1);
    }
    
    public boolean isEmpty(){
    	return size == 0;
    }
    
    public C getSquare(int pos){
    	if(pos < 0 || pos >=size) throw new IndexOutOfBoundsException();
    	return track[pos];
    }

	public void add(C elem){
		//if (size == track.length){ // espandi
			//track = Arrays.copyOf(track, track.length * 2);
		//}
		track[size] = elem;
		size++;
	}
	
	public void add(int index,C elem){
		if(index < 0 || index >=size) throw new IndexOutOfBoundsException();
		for(int i=size-1;i>=index;i--){
			track[i+1] = track[i];
		}
		track[index] = elem;
		size++;
	}
    
    public int size(){
    	return size;
    }
    
    
	

	@Override
	public Iterator<C> iterator() {
		return new TrackIterator();
	}

    
	private class TrackIterator implements Iterator<C>{
		
		private int corrente = -1;
		
		@Override
		public boolean hasNext() {
			if(corrente == -1) return size() > 0;
			return corrente < size()-1;
		}

		@Override
		public C next() {
			if(!hasNext()) throw new NoSuchElementException();
			corrente++;
			return track[corrente];
		}
		
	}
    

}
