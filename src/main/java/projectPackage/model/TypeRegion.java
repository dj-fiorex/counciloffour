package projectPackage.model;

/**
 * Created by Med on 09/05/2016.
 */
public enum TypeRegion {
    MOUNTAINS, HILLS, COAST;
}
