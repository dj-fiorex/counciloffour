package projectPackage.model;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import com.sun.corba.se.impl.orbutil.graph.GraphImpl;
import projectPackage.model.bonus.containers.NobilitySquare;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.Deck;
import projectPackage.model.cards.PoliticCard;
import projectPackage.model.track.square.MoneySquare;
import projectPackage.model.track.square.VictorySquare;
import projectPackage.model.utility.ColorParser;

import java.awt.*;


public class Player {

    private String name;
    private Color color;
    private int numAssistants;
    private MoneySquare moneyPosition;
    private VictorySquare victoryPosition;
    private NobilitySquare nobilityPosition;
    private Deck<PermitCard> permitDeck;
    private Deck<PoliticCard> politicDeck;
    private Deck<PermitCard> graveDeck;

    public Player(String name,Color c) {

        this.name = name;
		this.color = c;
        permitDeck = new Deck<>();
        politicDeck = new Deck<>();
        graveDeck = new Deck<>();
        //this.color=c;
    }

    public Deck<PermitCard> getPermitDeck() { return permitDeck; }

	public void setPermitDeck(Deck<PermitCard> permitDeck) {
		this.permitDeck = permitDeck;
	}

	public Deck<PermitCard> getGraveDeck() {
		return graveDeck;
	}

	public void setGraveDeck(Deck<PermitCard> graveDeck) {
		this.graveDeck = graveDeck;
	}

	public void addPermitCard(PermitCard pc){
		permitDeck.add(pc);
	}
	
	public Color getColor() {
		return color;
	}

	public String getName(){
    	return name;
    }
    
    public NobilitySquare getNobilityPosition() {
        return nobilityPosition;
    }

    public void setNobilityPosition(NobilitySquare nobilityPosition) {
        this.nobilityPosition = nobilityPosition;
    }

    public VictorySquare getVictoryPosition() {
        return victoryPosition;
    }

    public void setVictoryPosition(VictorySquare victoryPosition) {
        this.victoryPosition = victoryPosition;
    }

    public MoneySquare getMoneyPosition() {
        return moneyPosition;
    }

    public void setMoneyPosition(MoneySquare moneyPosition) {
        this.moneyPosition = moneyPosition;
    }
    
    
    public int getNumAssistant() {
        return numAssistants;
    }

    public void setNumAssistant(int numAssistant) {
        this.numAssistants = numAssistant;
    }
    
    
    public Deck<PoliticCard> getPoliticDeck() {
		return politicDeck;
	}

	public void setPoliticDeck(Deck<PoliticCard> politicDeck) {
		this.politicDeck = politicDeck;
	}

    public void payAssistant(int number) {
        numAssistants = numAssistants-number;
    }

    public void acquireAssistant (int number) {
        numAssistants = numAssistants+number;
    }
    
    public int hashCode(){
    	final int molt = 41; //numero primo
    	int i = molt*name.hashCode()+molt*color.hashCode();
    	return i;
    }
    
    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("Name: "+name+"\n");
    	sb.append("Values of player:\n");
    	sb.append("Color = "+ ColorParser.getStringFromColor(color)+"\n");
    	sb.append("Num assistants = "+numAssistants+"\n");
    	sb.append("Money position = "+moneyPosition.getPosition()+"\n");
    	sb.append("Victory position = "+victoryPosition.getPosition()+"\n");
    	sb.append("Nobility position = "+nobilityPosition.getPosition()+"\n");
    	sb.append("Yours politic cards:\n");
        StringBuilder sb2 = new StringBuilder(400);
        sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        sb2.append("++            ++            ++            ++            ++            ++            ++                             ++\n");
        for(PoliticCard p : politicDeck.getDeckCards()){
            sb2.append("++   " + ColorParser.getStringFromColor(p.getColorCard())+ "   ");
            //sb.append(p);
    		//sb.append("   ");
    	}
        sb2.append("++            ++            ++            ++            ++            ++            ++                                         ++\n");
        sb2.append("++            ++            ++            ++            ++            ++            ++                                         ++\n");
        sb2.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    	sb.append(sb2);
        sb.append("\n");
    	sb.append("Yours permit cards face up:\n");
    	for(PermitCard p : permitDeck.getDeckCards()){
    		sb.append(p);
    		sb.append("   ");
    	}
    	sb.append("\n");
    	sb.append("Yours permit cards face down:\n");
    	for(PermitCard p : graveDeck.getDeckCards()){
    		sb.append(p);
    		sb.append("   ");
    	}
        return sb.toString();
    }
    
}
