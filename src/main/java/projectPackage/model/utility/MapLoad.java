package projectPackage.model.utility;

import org.omg.CORBA.MARSHAL;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import projectPackage.model.City;
import projectPackage.model.TypeRegion;
import projectPackage.model.bonus.containers.*;
import projectPackage.model.bonus.Bonus;
import projectPackage.model.bonus.decorators.MoneyDecorator;
import projectPackage.model.bonus.decorators.VictoryDecorator;
import projectPackage.model.turn.TurnMachine;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by carme on 16/06/2016.
 */
public class MapLoad {
	
    private String pathName = "C://Users//Med//Desktop//map//carica.xml";
    private TurnMachine tm;

    public MapLoad(TurnMachine tm) {
        this.tm = tm;
    }

    public HashMap<City, LinkedList<City>> getMap() {
        City A;
        City B;
        City C;
        City D;
        City E;
        City F;
        City G;
        City H;
        City I;
        City J;
        City K;
        City L;
        City M;
        City N;
        City O;
        LinkedList<City> adiacentCity = new LinkedList<>();
        HashMap<City, LinkedList<City>> mappa = new HashMap<>();
        //String path = "C://Users//carme//Documents//test2//test//carica.xml";  //param
        try {
            //Leggo la mappa da file
            File inputFile = new File("C:\\Users\\Carmelo\\Downloads\\CouncilOfFour\\src\\main\\java\\projectPackage\\map\\carica.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList mapNodeList = doc.getElementsByTagName("City");
            for (int temp = 0; temp < mapNodeList.getLength(); temp++) {
                Node nNode = mapNodeList.item(temp);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    String name1 = eElement.getAttribute("Name");
                    String coloreinteoria = eElement.getAttribute("Color");
                    String typeBonus = eElement.getAttribute("Coin");
                    Color color1 = ColorParser.getColorFromString(coloreinteoria);
                    TypeRegion type1 = TypeRegion.valueOf(eElement.getAttribute("TypeRegion"));
                    Bonus b;
                    switch (typeBonus) {
                        case "Money": b = new MoneyDecorator(new BlankContainer(tm),10);
                        case "Victory": b = new VictoryDecorator(new BlankContainer(tm), 5);
                    }
                    City tempCity = new City(name1, color1, type1);
                    //tempCity.setCoin(b);   MED
                    //System.out.println(tempCity);
                    NodeList adiacent = eElement.getElementsByTagName("AdiacentCity");
                    //System.out.println(adiacent.getLength());
                    for (int temp3 = 0; temp3 < adiacent.getLength(); temp3++)
                    {
                        Node adiacentNode = adiacent.item(temp3);
                        if(adiacentNode.getNodeType() == Node.ELEMENT_NODE){
                            Element aElement = (Element) adiacentNode;
                            String name2 = aElement.getAttribute("Name");
                            Color color2 = ColorParser.getColorFromString(aElement.getAttribute("Color"));
                            TypeRegion type2 = TypeRegion.valueOf(aElement.getAttribute("TypeRegion"));
                            City tempAdiacentCity = new City(name2, color2, type2);
                            //System.out.println(tempAdiacentCity.toString());
                            adiacentCity.add(tempAdiacentCity);
                        }
                    }
                    mappa.put(tempCity,adiacentCity);
                    /*
                    for (City c: mappa.keySet()) {
                        System.out.println(c.getName());
                    }
					*/
                }
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            System.out.println(e.toString());
        }
        return mappa;
    }
}
