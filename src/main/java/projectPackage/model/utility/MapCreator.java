package projectPackage.model.utility;

import dnl.utils.text.table.TextTable;
import org.w3c.dom.*;
import projectPackage.model.City;
import projectPackage.model.TypeRegion;

import javax.swing.table.TableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;


/**
 * Created by carme on 14/06/2016.
 */
public class MapCreator {
    public static void main(String argv[]) {
        try {
            City A = new City("ARKON", Color.blue, TypeRegion.COAST);
            City B = new City("BURGEN", Color.yellow, TypeRegion.COAST);
            City C = new City("CASTRUM", Color.gray, TypeRegion.COAST);
            City D = new City("DORFUL", Color.gray, TypeRegion.COAST);
            City E = new City("ESTI", Color.orange, TypeRegion.COAST);
            City F = new City("FRAMEK", Color.yellow, TypeRegion.HILLS);
            City G = new City("GRADEN", Color.gray, TypeRegion.HILLS);
            City H = new City("HELLAR", Color.yellow, TypeRegion.HILLS);
            City I = new City("INDUR", Color.orange, TypeRegion.HILLS);
            City J = new City("JUVELAR", Color.magenta, TypeRegion.HILLS);
            City K = new City("KULTOS", Color.yellow, TypeRegion.MOUNTAINS);
            City L = new City("LYRAM", Color.gray, TypeRegion.MOUNTAINS);
            City M = new City("MERKATIM", Color.blue, TypeRegion.MOUNTAINS);
            City N = new City("NARIS", Color.orange, TypeRegion.MOUNTAINS);
            City O = new City("OSIUM", Color.yellow, TypeRegion.MOUNTAINS);

            HashMap<City, LinkedList<City>> mappaa = new HashMap<>();
            LinkedList<City> ACity = new LinkedList<City>();
            ACity.add(B);
            ACity.add(C);
            mappaa.put(A, ACity);
            LinkedList<City> BCity = new LinkedList<City>();
            BCity.add(D);
            BCity.add(A);
            BCity.add(E);
            mappaa.put(B, BCity);
            LinkedList<City> CCity = new LinkedList<City>();
            CCity.add(A);
            CCity.add(F);
            mappaa.put(C, CCity);
            //logger.log(Level.ALL, "C aggiunta");
            LinkedList<City> DCity = new LinkedList<City>();
            DCity.add(G);
            DCity.add(B);
            mappaa.put(D, DCity);
            //logger.log(Level.ALL, "D aggiunta");
            LinkedList<City> ECity = new LinkedList<City>();
            ECity.add(H);
            ECity.add(B);
            mappaa.put(E, ECity);
            //logger.log(Level.ALL, "E aggiunta");
            LinkedList<City> FCity = new LinkedList<City>();
            FCity.add(C);
            FCity.add(I);
            mappaa.put(F, FCity);
            //logger.log(Level.ALL, "F aggiunta");
            LinkedList<City> GCity = new LinkedList<City>();
            GCity.add(D);
            GCity.add(J);
            mappaa.put(G, GCity);
            //logger.log(Level.ALL, "G aggiunta");
            LinkedList<City> HCity = new LinkedList<City>();
            HCity.add(E);
            HCity.add(J);
            HCity.add(M);
            mappaa.put(H, HCity);
            //logger.log(Level.ALL, "H aggiunta");
            LinkedList<City> ICity = new LinkedList<City>();
            ICity.add(F);
            ICity.add(J);
            ICity.add(K);
            mappaa.put(I, ICity);
            //logger.log(Level.ALL, "I aggiunta");
            LinkedList<City> JCity = new LinkedList<City>();
            JCity.add(H);
            JCity.add(G);
            JCity.add(I);
            JCity.add(L);
            JCity.add(M);
            mappaa.put(J, JCity);
            //logger.log(Level.ALL, "J aggiunta");
            LinkedList<City> KCity = new LinkedList<City>();
            KCity.add(I);
            KCity.add(N);
            mappaa.put(K, KCity);
            //logger.log(Level.ALL, "K aggiunta");
            LinkedList<City> LCity = new LinkedList<City>();
            LCity.add(J);
            LCity.add(O);
            mappaa.put(L, LCity);
            //logger.log(Level.ALL, "L aggiunta");
            LinkedList<City> MCity = new LinkedList<City>();
            MCity.add(H);
            MCity.add(O);
            mappaa.put(M, MCity);
            //logger.log(Level.ALL, "M aggiunta");
            LinkedList<City> NCity = new LinkedList<City>();
            NCity.add(O);
            NCity.add(K);
            mappaa.put(N, NCity);
            //logger.log(Level.ALL, "N aggiunta");
            LinkedList<City> OCity = new LinkedList<City>();
            OCity.add(N);
            OCity.add(M);
            mappaa.put(O, OCity);

            DocumentBuilderFactory dcf = DocumentBuilderFactory.newInstance();
            DocumentBuilder dc = dcf.newDocumentBuilder();
            Document document = dc.newDocument();
            Element rootElement = document.createElement("Mappa");  //create root element Mappa
            document.appendChild(rootElement);  //add root element to the document
            LinkedList<City> adiacentCity = new LinkedList<>();
            HashMap<City, LinkedList<City>> coastAdiacentCity = new HashMap<>();
            HashMap<City, LinkedList<City>> hillsAdiacentCity = new HashMap<>();
            HashMap<City, LinkedList<City>> mountainsAdiacentCity = new HashMap<>();
            for (City c : mappaa.keySet()) //for every key in mappa
            {
                switch (c.getTypeRegion()) {
                    case COAST:
                        adiacentCity = mappaa.get(c);
                        coastAdiacentCity.put(c, adiacentCity);
                        break;
                    case HILLS:
                        adiacentCity = mappaa.get(c);
                        hillsAdiacentCity.put(c, adiacentCity);
                        break;
                    case MOUNTAINS:
                        adiacentCity = mappaa.get(c);
                        mountainsAdiacentCity.put(c, adiacentCity);
                        break;
                }
            }


            //Element coastRegion = document.createElement("RegionCOAST");
            //rootElement.appendChild(coastRegion);
            for (City c : coastAdiacentCity.keySet()) {
                Element cityNode = document.createElement("City"); //create a new node named City
                Attr name = document.createAttribute("Name");
                name.setValue(c.getName());
                cityNode.setAttributeNode(name);
                Attr color = document.createAttribute("Color");                //create color attribute
                color.setValue(projectPackage.model.utility.ColorParser.getStringFromColor(c.getCityColor()));        //set color attribute by calling an utility method
                cityNode.setAttributeNode(color);                                 //set attribute to the city
                Attr typeRegion = document.createAttribute("TypeRegion");
                typeRegion.setValue(c.getTypeRegion().toString());
                cityNode.setAttributeNode(typeRegion);
                rootElement.appendChild(cityNode);                                //add the new node city to the document
                //Element adiacent = document.createElement("AdiacentCities:");  //create a new node named AdiacentCity
                //cityNode.appendChild(adiacent);                                   //add the new node to the map
                for (City cc : coastAdiacentCity.get(c)) {
                    Element adiacentCities = document.createElement("AdiacentCity");      //create a new node for each adiacent city
                    Attr namee = document.createAttribute("Name");
                    namee.setValue(cc.getName());
                    adiacentCities.setAttributeNode(namee);
                    Attr col = document.createAttribute("Color");                       //set color property
                    col.setValue(ColorParser.getStringFromColor(cc.getCityColor()));
                    adiacentCities.setAttributeNode(col);
                    Attr region = document.createAttribute("TypeRegion");
                    region.setValue(cc.getTypeRegion().toString());
                    adiacentCities.setAttributeNode(region);
                    cityNode.appendChild(adiacentCities);
                }
            }

            //Element hillsRegion = document.createElement("RegionHILLS");
            //rootElement.appendChild(hillsRegion);
            for (City c : hillsAdiacentCity.keySet()) {
                Element cityNode = document.createElement("City");
                Attr name = document.createAttribute("Name");
                name.setValue(c.getName());
                cityNode.setAttributeNode(name);
                Attr color = document.createAttribute("Color");                //create color attribute
                color.setValue(ColorParser.getStringFromColor(c.getCityColor()));        //set color attribute by calling an utility method
                cityNode.setAttributeNode(color);                                 //set attribute to the city
                Attr typeRegion = document.createAttribute("TypeRegion");
                typeRegion.setValue(c.getTypeRegion().toString());
                cityNode.setAttributeNode(typeRegion);
                rootElement.appendChild(cityNode);
                //Element adiacent = document.createElement("AdiacentCities:");
                //cityNode.appendChild(adiacent);
                for (City cc : hillsAdiacentCity.get(c)) {
                    Element adiacentCities = document.createElement("AdiacentCity");
                    Attr namee = document.createAttribute("Name");
                    namee.setValue(cc.getName());
                    adiacentCities.setAttributeNode(namee);
                    Attr col = document.createAttribute("Color");                       //set color property
                    col.setValue(ColorParser.getStringFromColor(cc.getCityColor()));
                    adiacentCities.setAttributeNode(col);
                    Attr region = document.createAttribute("TypeRegion");
                    region.setValue(cc.getTypeRegion().toString());
                    adiacentCities.setAttributeNode(region);
                    cityNode.appendChild(adiacentCities);
                }
            }

            //Element mountainRegion = document.createElement("RegionMOUNTAIN");
            //rootElement.appendChild(mountainRegion);
            for (City c : mountainsAdiacentCity.keySet()) {
                Element cityNode = document.createElement("City");
                Attr name = document.createAttribute("Name");
                name.setValue(c.getName());
                cityNode.setAttributeNode(name);
                Attr color = document.createAttribute("Color");                //create color attribute
                color.setValue(ColorParser.getStringFromColor(c.getCityColor()));        //set color attribute by calling an utility method
                cityNode.setAttributeNode(color);                                 //set attribute to the city
                Attr typeRegion = document.createAttribute("TypeRegion");
                typeRegion.setValue(c.getTypeRegion().toString());
                cityNode.setAttributeNode(typeRegion);
                rootElement.appendChild(cityNode);
                //Element adiacent = document.createElement("AdiacentCities:");
                //cityNode.appendChild(adiacent);
                for (City cc : mountainsAdiacentCity.get(c)) {
                    Element adiacentCities = document.createElement("AdiacentCity");
                    Attr namee = document.createAttribute("Name");
                    namee.setValue(cc.getName());
                    adiacentCities.setAttributeNode(namee);
                    Attr col = document.createAttribute("Color");                       //set color property
                    col.setValue(ColorParser.getStringFromColor(cc.getCityColor()));
                    adiacentCities.setAttributeNode(col);
                    Attr region = document.createAttribute("TypeRegion");
                    region.setValue(cc.getTypeRegion().toString());
                    adiacentCities.setAttributeNode(region);
                    cityNode.appendChild(adiacentCities);
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("C://Users//Lorenzo//git//CouncilOfFour//src//main//java//projectPackage//maptest.xml"));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");                             //print with indent
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");     //set indent
            transformer.transform(source, result);
            // Output to console for testing
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);



        }
        catch (ParserConfigurationException | TransformerException e)
        {
            System.out.println(e.toString());
        }
    }
}
