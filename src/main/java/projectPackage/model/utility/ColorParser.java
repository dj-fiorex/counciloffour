package projectPackage.model.utility;



import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by carme on 15/06/2016.
 */
public final class ColorParser {
    private static Map<Color, String> colorMap = new HashMap<>();
    private static final String black = "Black";
    private static final String yellow = "Yellow";
    private static final String blue = "Blue";
    private static final String gray = "Gray";
    private static final String orange = "Orange";
    private static final String magenta = "Magenta";
    private static final String red = "Red";
    private static final String white = "White";

    public static String getStringFromColor(Color col) {
        colorMap.put(Color.BLACK, black);
        colorMap.put(Color.YELLOW, yellow);
        colorMap.put(Color.BLUE, blue);
        colorMap.put(Color.GRAY, gray);
        colorMap.put(Color.ORANGE, orange);
        colorMap.put(Color.MAGENTA, magenta);
        colorMap.put(Color.RED, red);
        colorMap.put(Color.WHITE, white);
        for (Color c : colorMap.keySet()
                ) {
            if (col == c) return colorMap.get(c);
        }
        return "illegal";
    }

    public static Color getColorFromString(String s) {
        colorMap.put(Color.BLACK, black);
        colorMap.put(Color.YELLOW, yellow);
        colorMap.put(Color.BLUE, blue);
        colorMap.put(Color.GRAY, gray);
        colorMap.put(Color.ORANGE, orange);
        colorMap.put(Color.MAGENTA, magenta);
        colorMap.put(Color.RED, red);
        colorMap.put(Color.WHITE, white);
        for (Color c : colorMap.keySet()) {
            if (s.equals(colorMap.get(c))) return c;
        }
        //throw new NullPointerException("Colore non trovato");
        return Color.BLACK;
    }

}
