package projectPackage.model;

import projectPackage.model.bonus.containers.Coin;
import projectPackage.model.utility.ColorParser;

import java.awt.*;
import java.util.ArrayList;


public class City {

    private String name;
    private Color cityColor;
    private ArrayList<Emporium> emporiums;
    private char shortName;
    private TypeRegion typeRegion;
    private Coin coin;

    public City(String name, Color cityColor, TypeRegion typeRegion) {
        //per avere la prima lettera maiuscola:
        char c = name.charAt(0);
        String s1 = String.valueOf(c).toUpperCase();
        String s2 = name.substring(1,name.length());
        this.name = s1+s2;
        //
        this.shortName = this.name.charAt(0);
        this.cityColor = cityColor;
        this.typeRegion = typeRegion;
    }

    public Coin getCoin() {
        return coin;
    }
    public void setCoin(Coin coin) {
        this.coin = coin;
    }
    public TypeRegion getTypeRegion() {
        return typeRegion;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Color getCityColor() {
        return cityColor;
    }
    public char getShortName(){
        return shortName;
    }
    public void setCityColor(Color cityColor) {
        this.cityColor = cityColor;
    }
    public void build(Color color) throws CloneNotSupportedException {
        emporiums.add(new Emporium(color));
        coin.activateBonus();
    }
    public boolean contains(Color color) {
        return  emporiums.contains(new Emporium(color));
    }

    public int numberOfOthersEmporiums(Color color){
        int i=0;
        for(Emporium e: emporiums){
            if(!e.getColor().equals(color)) i++;
        }
        return i;
    }

    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("**************************************\n");
    	sb.append("*                                    *\n");
    	sb.append("*         Name: "+name+"             *\n");
    	sb.append("*        Color: "+ ColorParser.getStringFromColor(cityColor)+"                *\n");
    	sb.append("*  Region type: "+typeRegion+"                *\n");
    	sb.append("*                                    *\n");
    	sb.append("**************************************\n");
    	sb.append("City's emporiums: \n");
    	//for(Emporium e : emporiums){
    	//	sb.append(e);
    	//	sb.append("   ");
    	//}
    	return sb.toString();
    }

}

