package projectPackage.model.cards;


import java.awt.*;
import projectPackage.model.utility.ColorParser;

/**
 * Created by Med on 10/05/2016.
 */
public class PoliticCard implements Card {

    private Color colorCard;

    public PoliticCard(Color colorCard) {
		super();
		this.colorCard = colorCard;
	}

	public Color getColorCard() {
        return colorCard;
    }
    
    public String toString(){
		StringBuilder sb = new StringBuilder(200);
    	sb.append("*********\n");
    	sb.append("*       *\n");
    	sb.append("* "+ColorParser.getStringFromColor(colorCard)+" *\n");
    	sb.append("*       *\n");
    	sb.append("*********");
    	return sb.toString();
	}
    
}
