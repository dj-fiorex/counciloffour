package projectPackage.model.cards;

import dnl.utils.text.table.TextTable;

import java.awt.Color;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by Med on 15/05/2016.
 */

public class Deck <C extends Card> implements Iterable<C>{

    private LinkedList<C> deckCards;
    
    public Deck(){
    	deckCards = new LinkedList<C>();
    }
    
    public boolean isEmpty(){
    	return deckCards.isEmpty();
    }

    public void shuffle() {
    	Collections.shuffle(deckCards);
    }
    
    public void add(C c){
    	deckCards.add(c);
    }

    public void add(LinkedList<C> l){
    	for(C c: l){
    		deckCards.add(c);
    	}
    }
    
	public LinkedList<C> getDeckCards() {
		return deckCards;
	}

	public void setDeckCards(LinkedList<C> deckCards) {
		this.deckCards = deckCards;
	}
	
	public C getCard(int position){
		return deckCards.get(position);
	}
	
	public C getFirst(){
		return deckCards.getFirst();
	}
	
	public C getLast(){
		return deckCards.getLast();
	}
	
	public C removeFirst(){
		return deckCards.removeFirst();
	}
	
	public C removeLast(){
		return deckCards.removeLast();
	}

	public C removeCard(int position) {
		return deckCards.remove(position);
	}

	public String toString(){
		/*
		int numCards = deckCards.size();
		int i=0;
		String[] columnNames = new String[numCards];
		Object[][] ob = new Object[1][numCards];
		for (C c : deckCards){
			columnNames[i]="Card"+1+i;
			ob[0][i]= c;
			i++;
		}
		TextTable tt = new TextTable(columnNames, ob);
		//tt.setSort(0);
		tt.printTable();
		return null;
		//StringBuilder sb = new StringBuilder(200);
		//for(C c : deckCards){
			//System.out.print(c);

			//sb.append(c);
			//sb.append("  ");
		//}
		*/
		return deckCards.toString();
	}

	public Iterator<C> iterator(){
		return deckCards.iterator();
	}

	public int size() {
		return deckCards.size();
	}
}
