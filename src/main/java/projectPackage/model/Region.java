package projectPackage.model;

import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.bonus.containers.RegionCard;
import projectPackage.model.cards.Deck;

/**
 * Created by Med on 09/05/2016.
 */
public class Region {

    private TypeRegion type;

    private Council regionCouncil;

    private RegionCard regionPrize;

    private Deck<PermitCard> regionDeck;
    
    private PermitCard firstPermitCard;
    
    private PermitCard secondPermitCard;

    /*
	private LinkedList<City> regionCities=new LinkedList<City>();

	public LinkedList<City> getRegionCities() {
		return regionCities;
	}

	public void setRegionCities(LinkedList<City> regionCities) {
		this.regionCities = regionCities;
	}

     */
    
	public Region(TypeRegion type, Council regionCouncil) {
		super();
		this.type = type;
		this.regionCouncil = regionCouncil;
	}

	public PermitCard getFirstPermitCard() {
		return firstPermitCard;
	}

	public void setFirstPermitCard(PermitCard firstPermitCard) {
		this.firstPermitCard = firstPermitCard;
	}

	public PermitCard getSecondPermitCard() {
		return secondPermitCard;
	}

	public void setSecondPermitCard(PermitCard secondPermitCard) {
		this.secondPermitCard = secondPermitCard;
	}

	public TypeRegion getType() {
		return type;
	}

	public Council getRegionCouncil() {
		return regionCouncil;
	}

	public void setRegionCouncil(Council regionCouncil) {
		this.regionCouncil = regionCouncil;
	}

	public RegionCard getRegionPrize() {
		return regionPrize;
	}

	public void setRegionPrize(RegionCard regionPrize) {
		this.regionPrize = regionPrize;
	}

	public Deck<PermitCard> getRegionDeck() {
		return regionDeck;
	}
	
	//pesco una carta permesso dal mazzo e la ritorno:
	public PermitCard pickACard(){
		if(regionDeck.isEmpty()) return null;
		return regionDeck.removeFirst();
	}

	public void replaceACard(){
		if (this.firstPermitCard == null) this.firstPermitCard = pickACard();
		else this.secondPermitCard = pickACard();
	}

	public void setRegionDeck(Deck<PermitCard> regionDeck) {
		this.regionDeck = regionDeck;
	}
    
    public void replaceCards() {
		regionDeck.add(firstPermitCard);
		regionDeck.add(secondPermitCard);
		firstPermitCard = pickACard();
		secondPermitCard = pickACard();
	}
    
    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("******************************************\n");
    	sb.append("*                  "+type+"              *\n");
    	sb.append("******************************************\n");
    	sb.append("Council of region: \n");
    	sb.append(regionCouncil+"\n");
    	sb.append("First permit card face up:\n");
    	sb.append(firstPermitCard+"\n");
    	sb.append("Second permit card face up:\n");
    	sb.append(secondPermitCard+"\n");
    	return sb.toString();
    }

}
