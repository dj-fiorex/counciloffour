package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.model.*;
import projectPackage.model.bonus.containers.PermitCard;
import projectPackage.model.cards.*;
import projectPackage.view.messages.*;

import java.util.*;

public class MainState extends TurnState {

    public MainState(TurnMachine tm) {
        super(tm);
    }


    @Override
    public void start(Action a) throws CloneNotSupportedException {
		if (!tm.isQuick4() || !tm.isActionBonus()) tm.setMainExecuted(true);
    	if(a instanceof MainAction1) {
    		//if(tm.isQuickExecuted()) tm.setMainExecuted(true);
    		mainAction1(a);
    	}
    	if(a instanceof MainAction2 ){
    		mainAction2(a);
    	}
    	if(a instanceof MainAction3) {
    		mainAction3(a);
    	}
    	if(a instanceof MainAction4) {
    		mainAction4(a);
    	}
		System.out.println(tm.getGameBoard().getCurrPlayer());
    }


	@Override
	public void nextState() throws  CloneNotSupportedException {
		if (tm.isQuick4()) {
			tm.setQuick4(false);
			if (!tm.isMainExecuted()) {
				tm.notifyObservers(new AskMainActionMessage());
			}
			else {
				tm.setCurrentState(new EndState(tm));
				tm.notifyObservers(new GoToEndMessage(tm.getGameBoard().getCurrPlayer()));
			}
		}
		else if (tm.isActionBonus()) {

			//NON DEVO CAMBIARE STATO O FARE UNA NOTIFY.. IL METODO FINISCE QUI

		}
		else if (!tm.isQuickExecuted() || !tm.isJumpQuickAction()) {
			tm.setCurrentState(new QuickState(tm));
			tm.notifyObservers(new AskQuickActionMessage());
		}
		else {
			tm.setCurrentState(new EndState(tm));
			tm.notifyObservers(new GoToEndMessage(tm.getGameBoard().getCurrPlayer()));
		}
	}
	
	
	
	private void mainAction1(Action a) {
		MainAction1 ma = (MainAction1)a;
		int whichCouncillor = ma.getWhichCouncillor();
		int whichCouncil = ma.getWhichCouncil();
		ArrayList<Councillor> reserveOfCouncillors = tm.getGameBoard().getReserveOfCouncillors();
		Councillor toAdded = reserveOfCouncillors.remove(whichCouncillor-1);
		Councillor removedCouncillor;
		Council modifiedCouncil;
		if(whichCouncil == 4){
			modifiedCouncil = tm.getGameBoard().getKingCouncil();
			removedCouncillor = modifiedCouncil.removeLast();
			modifiedCouncil.addFirst(toAdded);
			tm.getGameBoard().setKingCouncil(modifiedCouncil);
		}
		else{
			modifiedCouncil = tm.getGameBoard().getRegion(whichCouncil).getRegionCouncil();// sono in alias? se si non c'e bisogno del set
			removedCouncillor = modifiedCouncil.removeLast();
			modifiedCouncil.addFirst(toAdded);
			tm.getGameBoard().getRegion(whichCouncil).setRegionCouncil(modifiedCouncil);
		}
		reserveOfCouncillors.add(removedCouncillor);
		tm.getGameBoard().setReserveOfCouncillors(reserveOfCouncillors);
		//guadagnare 4 monete:
		int i = tm.getGameBoard().getCurrPlayer().getMoneyPosition().getPosition();
		if (i>16) tm.getGameBoard().getCurrPlayer().setMoneyPosition(tm.getGameBoard().getMoneyTrack().getSquare(20));
		else tm.getGameBoard().getCurrPlayer().setMoneyPosition(tm.getGameBoard().getMoneyTrack().getSquare(i+4));
		//bisogna modificare anche il percorso ricchezza nel GameBoard?
		System.out.println(modifiedCouncil);
		System.out.println("Main Action 1 done");
	}

	private void mainAction2(Action a) throws CloneNotSupportedException {
		MainAction2 ma = (MainAction2)a;
		int cont = 0;
		LinkedList<Councillor> councillors = tm.getGameBoard().getRegion(ma.getWhichCouncilRegion()).getRegionCouncil().getCouncillors();
		LinkedList<PoliticCard> politicCards = tm.getGameBoard().getCurrPlayer().getPoliticDeck().getDeckCards();
		LinkedList<PoliticCard> discardedCards = tm.getGameBoard().getReserveOfPoliticCards().getDeckCards(); // lista di carte politica scartate dalla mano del giocatore
		for (Councillor c : councillors) {
			for (PoliticCard pc : politicCards) {
				if (pc.getColorCard().equals(c.getColor())) {
					cont++;
					politicCards.remove(pc);
					discardedCards.add(pc);
					break;
				}
			}
		}
		
		//aggiorno il deck di carte politica se sono in alias non c'e' bisogno
		//tm.getGameBoard().getCurrPlayer().getPoliticDeck().setDeckCards(politicCards);

		//rimetto quelle scartate nella riserva del gameboard:
		//tm.getGameBoard().putDiscardedPoliticCards(discardedCards);
		//consumo eventuale di monete:
		switch(cont){
			case 1: tm.getGameBoard().moveOnMoneyTrack(-10);
			case 2: tm.getGameBoard().moveOnMoneyTrack(-7);
			case 3: tm.getGameBoard().moveOnMoneyTrack(-4);
		}
		//Selezione Permit Card:
		PermitCard pc;
		if(ma.getWhichPermitCard() == 1) pc = tm.getGameBoard().getRegion(ma.getWhichCouncilRegion()).getFirstPermitCard();
		else pc = tm.getGameBoard().getRegion(ma.getWhichCouncilRegion()).getSecondPermitCard();
		tm.getGameBoard().getCurrPlayer().addPermitCard(pc);
		//rimpiazzo la permit card appena pescata con la prima del regionDeck:
		tm.getGameBoard().getRegion(ma.getWhichCouncilRegion()).replaceACard();
		//pc.activateBonus(); come si attivano i bonus med?
        pc.activateBonus();
		System.out.println("Main Action 2 done");
	}
	
	private void mainAction3(Action a) throws CloneNotSupportedException {
		MainAction3 ma = (MainAction3) a; 
		Player player = tm.getGameBoard().getCurrPlayer();
		PermitCard pc = player.getPermitDeck().removeCard(ma.getWhichPrivatePermitCard());
		player.getGraveDeck().add(pc);
		tm.getGameBoard().getCity(ma.getWhichCharacter()).build(player.getColor());// gia compreso il consumo di aiutanti per ogni altro emporio costruito degli altri giocatori
		System.out.println("Main Action 3 done");
	}
	
	@SuppressWarnings("unused")
	private void mainAction4(Action a){
		MainAction4 ma = (MainAction4) a;
		int cont = 0;
		LinkedList<Councillor> councillors = tm.getGameBoard().getKingCouncil().getCouncillors();
		LinkedList<PoliticCard> politicCards = tm.getGameBoard().getCurrPlayer().getPoliticDeck().getDeckCards();
		LinkedList<PoliticCard> discardedCards = tm.getGameBoard().getReserveOfPoliticCards().getDeckCards(); // lista di carte politica scartate dalla mano del giocatore
		for (Councillor c : councillors) {
			for (PoliticCard pc : politicCards) {
				if (pc.getColorCard().equals(c.getColor())) {
					cont++;
					politicCards.remove(pc);
					discardedCards.add(pc);
					break;
				}
			}
		}
		//aggiorno il deck di carte politica se sono in alias non c'e bisogno
		//tm.getGameBoard().getCurrPlayer().getPoliticDeck().setDeckCards(politicCards);
		//rimetto quelle scartate nella riserva del gameboard:
		//tm.getGameBoard().putDiscardedPoliticCards(discardedCards);
		//consumo eventuale di monete:
		switch(cont){
			case 1: tm.getGameBoard().moveOnMoneyTrack(-10);
			case 2: tm.getGameBoard().moveOnMoneyTrack(-7);
			case 3: tm.getGameBoard().moveOnMoneyTrack(-4);
		}
		System.out.println("Main Action 4 done");
	}



}
