package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.controller.exceptions.ActionNotAllowedException;
import projectPackage.model.GameBoard;
import projectPackage.view.messages.*;

/**
 * Created by Med on 24/05/2016.
 */
public class BeginState extends TurnState {
	
	

    public BeginState(TurnMachine tm) {
       super(tm);
    }

	private int choice;

	@Override
	public void start(Action a) { //questa azione "a" e' quella che avverte la macchina che ci troviamo nello stato iniziale ed e' necessario far partire la richiesta di quale delle 4 azioni eseguire
		BeginAction ba = (BeginAction) a;
		//Fase 1: pesca una carta politica
		tm.getGameBoard().drawCard();
		if(ba.getChoiceAction() == 1) {
			tm.setMainOrQuick(true);
		}
		else {
			tm.setMainOrQuick(false);
		}

	}

	@Override
	public void nextState() throws CloneNotSupportedException {
		if(tm.isMainOrQuick()) {
			tm.setCurrentState(new MainState(tm));
			tm.notifyObservers(new AskWhichActionMessage(1));
		}
		else  {
			tm.setCurrentState(new QuickState(tm));
			tm.notifyObservers(new AskWhichActionMessage(2));
		}
	}
	
	


}
