package projectPackage.model.turn;

import projectPackage.controller.action.Action;
import projectPackage.model.GameBoard;

/**
 * Created by Med on 24/05/2016.
 */
public abstract class TurnState {

    protected TurnMachine tm;
    
    
    public abstract void start(Action a) throws CloneNotSupportedException;
    public abstract void nextState() throws CloneNotSupportedException;
    
    public TurnState(TurnMachine tm) {
    	this.tm=tm;
    }

}


