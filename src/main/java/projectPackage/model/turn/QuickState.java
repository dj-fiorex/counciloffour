package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.controller.exceptions.ActionNotAllowedException;
import projectPackage.model.Player;
import projectPackage.view.messages.*;


public class QuickState extends TurnState {

    public QuickState(TurnMachine tm) {
        super(tm);
    }

	@Override
	public void start(Action a) {
		if(a instanceof EndAction){
			tm.setJumpQuickAction(true);
		}
		else{
			tm.setQuickExecuted(true);
			if(a instanceof QuickAction1){
				//if(tm.isMainExecuted()) tm.setQuickExecuted(true);
				quickAction1(a);
			}
			if(a instanceof QuickAction2){
				quickAction2(a);
			}
			if(a instanceof QuickAction3){
				quickAction3(a);
			}
			if(a instanceof QuickAction4){
				quickAction4(a);
			}
		}
		System.out.println(tm.getGameBoard().getCurrPlayer());

	}

	@Override
	public void nextState() throws CloneNotSupportedException {
		if (tm.isQuick4() || !tm.isMainExecuted()) {
			tm.setCurrentState(new MainState(tm));
			tm.notifyObservers(new AskMainActionMessage());
		}
		else {
			tm.setCurrentState(new EndState(tm));
			tm.notifyObservers(new GoToEndMessage(tm.getGameBoard().getCurrPlayer()));
		}
	}

	private void quickAction1(Action a) {
		Player player = tm.getGameBoard().getCurrPlayer();
		player.acquireAssistant(1);
		tm.getGameBoard().moveOnMoneyTrack(-3);
		System.out.println("Quick Action 1 done");
	}

    private void quickAction2(Action a) {
		QuickAction2 quickAction2 = (QuickAction2) a;
		tm.getGameBoard().getCurrPlayer().payAssistant(1);
		int choice = quickAction2.getWhichRegion();
		tm.getGameBoard().getRegion(choice).replaceCards();
		System.out.println("Quick Action 2 done");
	}

	private void quickAction3(Action a) {
		tm.getGameBoard().getCurrPlayer().payAssistant(1);
		int region = ((QuickAction3) a).getWhichRegion();
		int councillor = ((QuickAction3) a).getWhichCouncillor();
		tm.getGameBoard().corruptCouncil(region, councillor);
		System.out.println("Quick Action 3 done");
	}

	private void quickAction4(Action a) {
		tm.setQuick4(true);
	}
}
