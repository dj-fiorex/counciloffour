package projectPackage.model.turn;

import java.awt.Color;

import projectPackage.Observable;
import projectPackage.controller.action.Action;
import projectPackage.model.City;
import projectPackage.model.GameBoard;
import projectPackage.model.Player;
import projectPackage.model.Region;
import projectPackage.model.bonus.*;
import projectPackage.model.bonus.containers.*;
import projectPackage.model.bonus.decorators.*;
import projectPackage.model.cards.Deck;
import projectPackage.model.track.Track;


public class TurnMachine extends Observable{
	
	private boolean mainExecuted, quickExecuted, jumpQuickAction, mainOrQuick, quick4, actionBonus;
	
	private int counterMarket = 1;

	private int counterBuySell = 0;

    private TurnState currentState;

    private GameBoard gameBoard;

	public boolean isActionBonus() {
		return actionBonus;
	}

	public void setActionBonus(boolean actionBonus) {
		this.actionBonus = actionBonus;
	}

	public TurnMachine() {
        currentState  = new InitState(this);
        this.mainExecuted = false;
        this.quickExecuted = false;
        this.jumpQuickAction = false;
		this.mainOrQuick = false;
		this.quick4 = false;
		this.actionBonus = false;
		gameBoard = new GameBoard();
    }

	public boolean isJumpQuickAction() {
		return jumpQuickAction;
	}

	public void setJumpQuickAction(boolean jumpQuickAction) {
		this.jumpQuickAction = jumpQuickAction;
	}

	public boolean isMainOrQuick() {
		return mainOrQuick;
	}

	public void setMainOrQuick(boolean mainOrQuick) {
		this.mainOrQuick = mainOrQuick;
	}

	public boolean isMainExecuted() {
		return mainExecuted;
	}

	public void setMainExecuted(boolean mainExecuted) {
		this.mainExecuted = mainExecuted;
	}

	public boolean isQuickExecuted() {
		return quickExecuted;
	}

	public void setQuickExecuted(boolean quickExecuted) {
		this.quickExecuted = quickExecuted;
	}

	public TurnState getCurrentState(){
    	return currentState;
    }

	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}

	public GameBoard getGameBoard(){
    	return gameBoard;
    }
    
    public void start(Action a) throws CloneNotSupportedException{
    	currentState.start(a);
    	nextState();
    }
    
    public void nextState() throws CloneNotSupportedException{
    	currentState.nextState();
    }
    
    public void setCurrentState(TurnState currentState) {
		this.currentState = currentState;
	}

	public boolean isQuick4() {
		return quick4;
	}

	public void setQuick4(boolean quick4) {
		this.quick4 = quick4;
	}

	public Player nextPlayer(){
    	counterMarket++;
    	return gameBoard.nextPlayer();
    }
    
    
    public int getCounterMarket(){
    	return counterMarket;
    }

	public int getCounterBuySell() {
		return counterBuySell;
	}

	public void setCounterBuySell(int counterBuySell) {
		this.counterBuySell = counterBuySell;
	}

	public void setCounterMarket(int newCounterMarket){
    	this.counterMarket = newCounterMarket;
    }
    
    /*
	public void init(LinkedList<Player> players){
    	this.gameBoard = new GameBoard();
    	gameBoard.setPlayers(players);
    	gameBoard.setCurrPlayer(players.get(0));
    	//generazione mappa da file
    	gameBoard.generateBonus();
    	this.notifyObservers();
	}
	
	*/
    
    public void generateBonus(){
    	//inizializzazione bonus
    	//Coin:
    	Bonus b1 = new MoneyDecorator(new BlankContainer(this),10);
    	Bonus b2 = new AssistantDecorator(new BlankContainer(this),10);
    	Bonus b3 = new VictoryDecorator(new BlankContainer(this),10);
    	for(City c : gameBoard.getAllCities()){
    		c.setCoin(new Coin(b1));
    	}
    	//RegionCard
    	Region [] regions = gameBoard.getRegions();
    	char [] c1 = {'a','b','c'};
    	char [] c2 = {'d','e','f'};
    	char [] c3 = {'g','h','i'};
    	Deck<PermitCard> dpc = new Deck<PermitCard>();
    	dpc.add(new PermitCard(b1,c1));
    	dpc.add(new PermitCard(b2,c2));
    	dpc.add(new PermitCard(b3,c3));
    	dpc.add(new PermitCard(b1,c2));
		dpc.add(new PermitCard(b1,c2));
		dpc.add(new PermitCard(b1,c2));
    	for(int i=0;i<regions.length;i++){
    		regions[i].setRegionPrize(new RegionCard(b3));
    		regions[i].setRegionDeck(dpc);
    		regions[i].setFirstPermitCard(regions[i].pickACard());
    		regions[i].setSecondPermitCard(regions[i].pickACard());
    	}
    	//ColorCard:
    	gameBoard.setColorCards(new Deck<ColorCard>());
    	for(int i=0;i<4;i++){
    		gameBoard.getColorCards().add(new ColorCard(b3,Color.BLACK));
    	}
    	//KingCard
    	gameBoard.setKingCards(new Deck<KingCard>());
    	for(int i=0;i<4;i++){
    		gameBoard.getKingCards().add(new KingCard(b3));
    	}
    	//NobilitySquare
    	gameBoard.setNobilityTrack(new Track<NobilitySquare>(21));
    	for(int i=0;i<21;i++){
    		if(i == 4 || i == 12 || i == 17) gameBoard.getNobilityTrack().add(new NobilitySquare(b1,i));
    		else gameBoard.getNobilityTrack().add(new NobilitySquare(null,i));
    	}
    	for(Player p : gameBoard.getPlayers()){
    		p.setNobilityPosition(gameBoard.getNobilityTrack().getSquare(0));
    	}

    	
    }
	
	
	
}
