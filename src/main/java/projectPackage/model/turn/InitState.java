package projectPackage.model.turn;

import projectPackage.controller.action.*;
import projectPackage.controller.exceptions.ActionNotAllowedException;
import projectPackage.model.City;
import projectPackage.model.GameBoard;

import projectPackage.model.utility.MapLoad;
import projectPackage.view.messages.*;

public class InitState extends TurnState {
	
	public InitState(TurnMachine tm) {
		super(tm);
	}

	Message m = new AskActionMessage();

	@Override
	public void start(Action a) {
		InitAction ia = (InitAction)a;
        MapLoad ml = new MapLoad(tm);

		GameBoard gameBoard = tm.getGameBoard();
    	gameBoard.setPlayers(ia.getPlayers());
    	gameBoard.setCurrPlayer(ia.getPlayers().get(0));
        gameBoard.initializeMap(ml.getMap());		//generazione mappa da file
        tm.getGameBoard().factory();
    	tm.generateBonus();
	}

	@Override
	public void nextState() throws CloneNotSupportedException{
		tm.setCurrentState(new BeginState(tm));
		tm.notifyObservers(m);
	}

}
