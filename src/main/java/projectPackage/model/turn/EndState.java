package projectPackage.model.turn;

import projectPackage.controller.action.Action;
import projectPackage.controller.exceptions.ActionNotAllowedException;
import projectPackage.model.GameBoard;
import projectPackage.view.messages.*;

/**
 * Created by Med on 25/05/2016.
 */
public class EndState extends TurnState {

    public EndState(TurnMachine tm) {
        super(tm);
    }

	private int numPlayers = tm.getGameBoard().getNumPlayers();
	private int counterBuySell = tm.getCounterBuySell();

	@Override
	public void start(Action a) throws CloneNotSupportedException {
		tm.setMainExecuted(false);
		tm.setQuickExecuted(false);
		tm.setJumpQuickAction(false);
		tm.setMainOrQuick(false);
	}

	@Override
	public void nextState() throws CloneNotSupportedException {
		if(tm.getCounterMarket() == numPlayers){
			tm.setCurrentState(new MarketState(tm));
			if(counterBuySell<numPlayers){		// accade quando non tutti i giocatori hanno effettuato la vendita e l'acquisto
				tm.setCounterBuySell(counterBuySell+1);
				tm.notifyObservers(new SellMessage());
			} else {
				if(counterBuySell<(numPlayers*2)){		// accade quando non tutti i giocatori hanno effettuato l'acquisto ma tutti hannno effettuato la vendita
					tm.setCounterBuySell(counterBuySell+1);
					tm.notifyObservers(new BuyMessage());
				} else{								// accade quando tutti i giocatori hanno effettuato vendita e acquisto
					tm.setCounterMarket(1);
					tm.setCounterBuySell(0);
					tm.setCurrentState(new BeginState(tm));
					tm.getGameBoard().setCurrPlayer(tm.nextPlayer());
					tm.notifyObservers(new AskActionMessage());
				}

			}
		}
		else {
			tm.setCurrentState(new BeginState(tm));
			tm.getGameBoard().setCurrPlayer(tm.nextPlayer());
			tm.notifyObservers(new AskActionMessage());
		}
	}

   
}
