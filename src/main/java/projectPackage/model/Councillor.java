package projectPackage.model;

import projectPackage.model.utility.ColorParser;

import java.awt.*;

/**
 * Created by Med on 09/05/2016.
 */
public class Councillor {

    private Color color;

    public Councillor(Color color) {
		super();
		this.color = color;
	}

	public Color getColor() {
        return color;
    }

    
    public String toString(){
    	StringBuilder sb = new StringBuilder(200);
    	sb.append("********************\n");
    	sb.append("*     "+ ColorParser.getStringFromColor(color)+"    *\n");
    	sb.append("********************\n");
    	return sb.toString();
    }
}
