package projectPackage.model;

import projectPackage.model.bonus.containers.ColorCard;
import projectPackage.model.bonus.containers.KingCard;
import projectPackage.model.bonus.containers.NobilitySquare;
import projectPackage.model.cards.*;
import projectPackage.model.track.*;
import projectPackage.model.track.square.*;

import java.awt.*;
import java.util.*;


public class GameBoard extends projectPackage.Observable implements Cloneable {

    private King king = new King('J'); // citta' iniziale "Juvelar"
	
    private Region regions[] = new Region[3];

    private HashMap<City,LinkedList<City>> adjacentCities;

    private LinkedList<Player> players;

    private Player currPlayer;

    private ArrayList<Councillor> reserveOfCouncillors;

	private Council kingCouncil;

    private Track<NobilitySquare> nobilityTrack;

    private Track<VictorySquare> victoryTrack;

    private Track<MoneySquare> moneyTrack;
    
    private Deck<PoliticCard> reserveOfPoliticCards;

    private Deck<ColorCard> colorCards;
    
    private Deck<KingCard> kingCards;
    
    private LinkedList<String> randomPlayerExctraction; //tutti i nomi dei giocatori da estrarre
    
    public void factory(){
    	reserveOfCouncillors = new ArrayList<>(8);
    	reserveOfCouncillors.add(new Councillor(Color.BLACK));
    	reserveOfCouncillors.add(new Councillor(Color.WHITE));
    	reserveOfCouncillors.add(new Councillor(Color.RED));
    	reserveOfCouncillors.add(new Councillor(Color.BLUE));
    	reserveOfCouncillors.add(new Councillor(Color.WHITE));
    	reserveOfCouncillors.add(new Councillor(Color.WHITE));
    	reserveOfCouncillors.add(new Councillor(Color.WHITE));
    	reserveOfCouncillors.add(new Councillor(Color.WHITE));
    	//System.out.println(this.reserveOfCouncillors);
    	reserveOfPoliticCards = new Deck<>();
    	reserveOfPoliticCards.add(new PoliticCard(Color.GRAY));
    	reserveOfPoliticCards.add(new PoliticCard(Color.GRAY));
    	reserveOfPoliticCards.add(new PoliticCard(Color.GRAY));
    	reserveOfPoliticCards.add(new PoliticCard(Color.GRAY));
    	reserveOfPoliticCards.add(new PoliticCard(Color.GRAY));
    	LinkedList<TypeRegion> l1 = new LinkedList<>();
    	l1.add(TypeRegion.COAST);
    	l1.add(TypeRegion.HILLS);
    	l1.add(TypeRegion.MOUNTAINS);
    	LinkedList<Councillor> l2 = new LinkedList<>();
    	l2.add(new Councillor(Color.BLACK));
    	l2.add(new Councillor(Color.BLUE));
    	l2.add(new Councillor(Color.WHITE));
    	l2.add(new Councillor(Color.RED));
    	Council c1 = new Council(l2,TypeCouncil.COAST);
    	Council c2 = new Council(l2,TypeCouncil.HILLS);
    	Council c3 = new Council(l2,TypeCouncil.MOUNTAINS);
    	LinkedList<Council> l3 = new LinkedList<>();
    	l3.add(c1);
    	l3.add(c2);
    	l3.add(c3);
        regions = new Region[3];
    	for(int i=0;i<regions.length;i++){
    		regions[i] = new Region(l1.get(i), l3.get(i));
    	}
    	kingCouncil = new Council(l2, TypeCouncil.KING);
    	victoryTrack = new Track<VictorySquare>(100);
    	moneyTrack = new Track<MoneySquare>(21);
    	for(int i=0;i<100;i++){
    		victoryTrack.add(new VictorySquare(i));
    	}
    	for(int i=0;i<21;i++){
    		moneyTrack.add(new MoneySquare(i));
    	}
        int i=0;
        PoliticCard p1 = new PoliticCard(Color.BLACK);
        PoliticCard p2 = new PoliticCard(Color.RED);
        PoliticCard p3 = new PoliticCard(Color.WHITE);
        PoliticCard p4 = new PoliticCard(Color.ORANGE);
        PoliticCard p5 = new PoliticCard(Color.BLACK);
        Deck<PoliticCard> dp = new Deck<>();
        dp.add(p1);
        dp.add(p2);
        dp.add(p3);
        dp.add(p4);
        dp.add(p5);
        for(Player p : players){
            p.setMoneyPosition(moneyTrack.getSquare(10+(i++)));
    		p.setVictoryPosition(victoryTrack.getSquare(0));
            p.setPoliticDeck(dp);

    	}
    }

    public LinkedList<Player> getPlayers() {
		return players;
	}

	public Deck<ColorCard> getColorCards() {
        return colorCards;
    }

    public void setColorCards(Deck<ColorCard> colorCards) {
        this.colorCards = colorCards;
    }

    public Deck<KingCard> getKingCards() {
        return kingCards;
    }

    public void setKingCards(Deck<KingCard> kingCards) {
        this.kingCards = kingCards;
    }
    
    public Deck<PoliticCard> getReserveOfPoliticCards(){
    	return reserveOfPoliticCards;
    }
    
    public int shortestPathOfKing(City dest){
    	return minPathOfKing(dest);
    	
    }
    
    public void setNobilityTrack(Track<NobilitySquare> nobilityTrack) {
		this.nobilityTrack = nobilityTrack;
	}

	public int minPathOfKing(City destination){
    	if(!adjacentCities.keySet().contains(destination)) throw new IllegalArgumentException();
    	City source = getCity(king.getPosition());
		if(source.equals(destination)) return 0;
		if(adjacentCities.get(source).contains(destination)) return 1;
		LinkedList<City> coda = new LinkedList<>();
		LinkedList<City> contaLivelliVisitati = new LinkedList<>();
		HashMap<City,Boolean> visitati = new HashMap<>();
		for(City c : adjacentCities.keySet()){
			visitati.put(c, false);
		}
		coda.addLast(source);
		contaLivelliVisitati.addAll(adjacentCities.get(source));
		visitati.put(source, true);
		int minDistanza = 0;
		boolean flag1 = false;
		while(!coda.isEmpty()){
			City car = coda.removeFirst();
			LinkedList<City> adiacenti = adjacentCities.get(car);
			for(City c : adiacenti){
				if(!visitati.get(c)){
					if(c.equals(destination)) {
						flag1 = true;
						break;
					}
					visitati.put(c, true);
					coda.addLast(c);
				}
			}
			boolean flag2 = true;
			for(City c : contaLivelliVisitati){
				if(!visitati.get(c)) flag2 = false; 
			}
			if(flag2){
				minDistanza++;
				LinkedList<City> supporto = new LinkedList<>();
				for(City c1 : contaLivelliVisitati){
					for(City c2 : adjacentCities.get(c1)){
						if(!supporto.contains(c2)) supporto.add(c2);
					}
				}
				contaLivelliVisitati.addAll(supporto);
			}
			if(flag1){
				minDistanza++;
				break;
			}
		}
		return minDistanza;
    }
    
    //restituisce le citta' adiacenti in cui ha gia' costruito un emporio

    public LinkedList<City> adjacentCitiesBonus(City c){
    	LinkedList<City> l = new LinkedList<>();
    	LinkedList<City> adjacent = adjacentCities.get(c);
    	for(City city : adjacent){
    		if(city.contains(currPlayer.getColor())) l.add(city);
    	}
    	return l;
    }
    
    public void putDiscardedPoliticCards(LinkedList<PoliticCard> l){
    	reserveOfPoliticCards.add(l);
    	reserveOfPoliticCards.shuffle();
    }
    
    public ArrayList<Councillor> getReserveOfCouncillors() {
		return reserveOfCouncillors;
	}

	public Region[] getRegions() {
		return regions;
	}

	public void setReserveOfCouncillors(ArrayList<Councillor> reserveOfCouncillors) {
		this.reserveOfCouncillors = reserveOfCouncillors;
	}


    public void initializeMap(HashMap<City,LinkedList<City>> map) {
    	this.adjacentCities = map;
    }

    public int getNumCity(){
    	return this.adjacentCities.keySet().size();
    }

    public Track<NobilitySquare> getNobilityTrack() {
        return nobilityTrack;
    }

    public Track<VictorySquare> getVictoryTrack() {
        return victoryTrack;
    }

    public Track<MoneySquare> getMoneyTrack() {
        return moneyTrack;
    }

    
    public int getNumPlayers() {
        return players.size();
    }

    public void setPlayers(LinkedList<Player> players) {
        this.players = players;
    }

    public Player getCurrPlayer() {
        return currPlayer;
    }

    public void setCurrPlayer(Player currPlayer) {
        this.currPlayer = currPlayer;
    }
    
    public Council getKingCouncil() {
		return kingCouncil;
	}
    
    public void setKingCouncil(Council kingCouncil) {
		this.kingCouncil = kingCouncil;
	}

    public Region getRegion(int choice) {
        return regions[choice - 1];
    }

    public Region getRegion(TypeRegion tc) {
        for (Region region : regions) {
            if (region.getType().equals(tc)) return region;
        }
        return null;
    }

	public Player nextPlayer(){
    	int i = players.indexOf(currPlayer);
    	if(i == players.size()) return players.get(0);
    	return players.get(i+1);
    }

	//dove si chiama il seguente metodo controllare che se il giocatore e' l'ultimo non ha piu' senso chiamare questo metodo
	public Player nextRandomPlayer(){
		int i = randomPlayerExctraction.size();
		int randomNumb = randInt(0,i);
		String s = randomPlayerExctraction.remove(randomNumb); //cosi' la lista si svuota e torna il nome del giocatore
		for(Player p : players){
			if(p.getName().equals(s)) return p;
		}
		return null;
	}

	public Player getPlayer(int pos){
		return players.get(pos);
	}

	//questo metodo genera un numero casuale tra min e max
	public int randInt(int min, int max) {
		  Random rand = new Random();
		  int randomNum = rand.nextInt((max - min) + 1) + min;
		  return randomNum;
	}

    public void moveOnMoneyTrack(int quantity) {
        int pos = currPlayer.getMoneyPosition().getPosition();
        if (pos == 20) return;
        int newPos;
        if (pos+quantity >= 20 ) newPos=20;
        else newPos=pos+quantity;
        currPlayer.setMoneyPosition(getMoneyTrack().getSquare(newPos));

    }

    public void moveOnVictoryTrack(int quantity) {
        int pos = currPlayer.getVictoryPosition().getPosition();
        if (pos == 99) return;
        int newPos;
        if (pos+quantity > 99 ) newPos=99;
        else newPos=pos+quantity;
        currPlayer.setVictoryPosition(getVictoryTrack().getSquare(newPos));

    }

    public void moveOnNobilityTrack(int quantity) throws CloneNotSupportedException {
        int pos = currPlayer.getNobilityPosition().getPosition();
        int newPos;
        if (pos+quantity >= 20 ) newPos=20;
        else newPos=pos+quantity;
        currPlayer.setNobilityPosition(getNobilityTrack().getSquare(newPos));
        currPlayer.getNobilityPosition().activateBonus();

    }

    public LinkedList<City> getAllCities(){
    	LinkedList<City> l = new LinkedList<>();
		for(City c : this.adjacentCities.keySet()){
			l.add(c); //sono in aliasing?
		}
		return l;
    }

    public void buildEmporium(char city) throws CloneNotSupportedException {
    	boolean flag = false;
        City c = getCity(city);
        c.build(currPlayer.getColor());
        currPlayer.setNumAssistant(currPlayer.getNumAssistant() - c.numberOfOthersEmporiums(currPlayer.getColor()));
        if (hasBuiltInAll(c.getTypeRegion())) {
            getRegion(c.getTypeRegion()).getRegionPrize().activateBonus();
            if (!kingCards.isEmpty()) {
                kingCards.getFirst().activateBonus();
                kingCards.removeFirst();
            }
        }
        if (hasBuiltInAll(c.getCityColor())) {
            getColorCard(c.getCityColor()).activateBonus();
            if (!kingCards.isEmpty()) {
                kingCards.getFirst().activateBonus();
                kingCards.removeFirst();
            }

        }
    }

    public boolean hasBuiltInAll(Color color) {
        LinkedList<City> cities = getSameColor(color);
        int cont=0;
        for (City c : cities) {
            if (alreadyBuilt(c.getShortName())) {
                cont++;
            }
        }
        if (cities.size() == cont) return true;
        else return false;
    }

    public boolean hasBuiltInAll(TypeRegion tc) {
        LinkedList<City> cities = getSameRegion(tc);
        int cont=0;
        for (City c : cities) {
            if (alreadyBuilt(c.getShortName())) {
                cont++;
            }
        }
        if (cities.size() == cont) return true;
        else return false;
    }

    public LinkedList<City> getSameRegion(TypeRegion tc) {
        LinkedList<City> cities = new LinkedList<City>();
        for (City c: adjacentCities.keySet()) {
            if (c.getTypeRegion().equals(tc)) {
                cities.add(c);
            }
        }
        return cities;
    }

    public LinkedList<City> getSameColor(Color color) { //perche' private med?
        LinkedList<City> cities = new LinkedList<City>();
        for (City c: adjacentCities.keySet()) {
            if (c.getCityColor().equals(color)) {
                cities.add(c);
            }
        }
        return cities;
    }

    public City getCity(char c) {
        for(City city : adjacentCities.keySet()){
        	if(city.getShortName() == c) return city;
        }
        return null;
    }

    public boolean containsShortCity(char c){
    	for(City city : adjacentCities.keySet()){
        	if(city.getShortName() == c) return true;
        }
    	return false;
    }

    public LinkedList<City> getCitiesOfPlayer() {
        LinkedList<City> list = new LinkedList<>();
        for (City c : adjacentCities.keySet()) {
            if (alreadyBuilt(c)) list.add(c);
        }
        return list;
    }

    public boolean alreadyBuilt(char city) {
        return getCity(city).contains(currPlayer.getColor());
    }

    public boolean alreadyBuilt(City city) {
        return city.contains(currPlayer.getColor());
    }

    public void corruptCouncil(int council, int councillor) {
        Councillor newCouncillor = getFromReserve(councillor);
        Councillor temp;

        if(council==4) {
            temp = getKingCouncil().corrupt(newCouncillor);
            reserveOfCouncillors.add(temp);
        }
        else {
            temp = getRegion(council).getRegionCouncil().corrupt(newCouncillor);
            reserveOfCouncillors.add(temp);
        }
    }

    public Councillor getFromReserve(int choice) {
        return reserveOfCouncillors.get(choice-1);
    }

    public void drawCard() {
        PoliticCard pc = reserveOfPoliticCards.removeFirst();
        currPlayer.getPoliticDeck().add(pc);
    }

    public ColorCard getColorCard(Color color) {
        for (ColorCard cc : colorCards) {
            if(cc.getColor().equals(color)) return cc;
        }
        return null;
    }


}

