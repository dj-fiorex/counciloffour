package projectPackage.controller;


import projectPackage.controller.action.*;
import projectPackage.model.turn.TurnMachine;
import projectPackage.view.View;
import projectPackage.view.messages.*;


public class Controller implements projectPackage.Observer {

    @SuppressWarnings("unused")
	private TurnMachine tm;
    @SuppressWarnings("unused")
	private View view;

    private ActionFactoryVisitorImpl visitor;

    public Controller(TurnMachine tm, View view) {
        this.tm=tm;
        this.view=view;
        visitor = new ActionFactoryVisitorImpl(this.tm);
    }

    @Override
    public <C> void update(C msg) throws CloneNotSupportedException {
    	Action action =((ViewControllerMessage) msg).createAction(visitor);
        action.execute();
        
    }

	
}
