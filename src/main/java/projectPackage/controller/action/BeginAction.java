package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;

public class BeginAction extends Action {
	
	private int choiceAction;
	

	public BeginAction(int choiceAction,TurnMachine tm) {
		super(tm);
		this.choiceAction = choiceAction;
	}


	@Override
	public void execute() throws CloneNotSupportedException {
		tm.start(this);
	}


	public int getChoiceAction() {
		return choiceAction;
	}

}
