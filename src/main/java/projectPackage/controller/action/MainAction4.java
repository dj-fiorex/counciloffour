package projectPackage.controller.action;

import java.util.LinkedList;

import projectPackage.controller.exceptions.*;
import projectPackage.model.*;
import projectPackage.model.cards.*;
import projectPackage.model.turn.TurnMachine;

public class MainAction4 extends Action {

    private char whichCity;
    
	public MainAction4(char whichCity, TurnMachine tm) {
		super(tm);
		this.whichCity = whichCity;
	}
 
    public int getWhichCity() {
		return whichCity;
	}


	@Override
    public void execute() throws CloneNotSupportedException {
		if(!enoughPoliticCards()) throw new NotEnoughPoliticCards();
		if(!enoughMoneyForTheWalk()) throw new NotEnoughMoneyForMove();
		if(!alreadyBuilt()) throw new AlreadyBuiltException();
		if(!enoughAssistant()) throw new NotEnoughAssistants();
    	tm.start(this);
    }
	
	private boolean alreadyBuilt(){
		return tm.getGameBoard().alreadyBuilt(whichCity);
	}
	
	private boolean enoughAssistant(){
		Player currPlayer = tm.getGameBoard().getCurrPlayer();
		return currPlayer.getNumAssistant() < tm.getGameBoard().getCity(whichCity).numberOfOthersEmporiums(currPlayer.getColor()) ? false : true;
	}
	
	@SuppressWarnings("unchecked")
	private boolean enoughPoliticCards(){
		Player currPlayer = tm.getGameBoard().getCurrPlayer();
		LinkedList<Councillor> councillors = tm.getGameBoard().getKingCouncil().getCouncillors();
		LinkedList<PoliticCard> politicCards = (LinkedList<PoliticCard>) currPlayer.getPoliticDeck().getDeckCards().clone();//sono in aliasing? se si devo fare clone
		int cont = 0;
		for(Councillor c : councillors){
			for(PoliticCard pc : politicCards){
				if(pc.getColorCard().equals(c.getColor())){
					cont++;
					politicCards.remove(pc);
					break;
				}
			}
		}
		if(cont < 1) return false;
		if(cont == 1 && currPlayer.getMoneyPosition().getPosition() < 10) return false;
		if(cont == 2 && currPlayer.getMoneyPosition().getPosition() < 7) return false;
		if(cont == 3 && currPlayer.getMoneyPosition().getPosition() < 4) return false;
		return true;
	}
	
	private boolean enoughMoneyForTheWalk(){
		City dest = tm.getGameBoard().getCity(this.whichCity);
		int toPay = 2*tm.getGameBoard().shortestPathOfKing(dest);
		if(tm.getGameBoard().getCurrPlayer().getMoneyPosition().getPosition() < toPay) return false;
		return true;
	}
	
}
