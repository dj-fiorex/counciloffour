package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;

public class EndAction extends Action {
    
	
	
	public EndAction(TurnMachine tm) {
		super(tm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute() throws CloneNotSupportedException {
		tm.start(this);
	}

}
