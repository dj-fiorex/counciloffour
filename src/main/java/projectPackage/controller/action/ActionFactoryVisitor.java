package projectPackage.controller.action;

import projectPackage.view.messages.*;

/**
 * Created by Med on 25/05/2016.
 */
public interface ActionFactoryVisitor {

	Action visit(InitMessage msg);
	Action visit(BeginMessage msg);
	Action visit(NextTurnMessage msg);
	Action visit(EndActionMessage msg);
	Action visit(MainAction1Message msg);
	Action visit(MainAction2Message msg);
    Action visit(MainAction3Message msg);
    Action visit(MainAction4Message msg);
    Action visit(QuickAction1Message msg);
    Action visit(QuickAction2Message msg);
    Action visit(QuickAction3Message msg);
    Action visit(QuickAction4Message msg);

}
