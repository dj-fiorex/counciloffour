package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;
import projectPackage.view.messages.*;


public class ActionFactoryVisitorImpl implements ActionFactoryVisitor {
    
	private TurnMachine tm;
	
	public ActionFactoryVisitorImpl(TurnMachine tm) {
		super();
		this.tm = tm;
	}

	@Override
    public Action visit(InitMessage msg){
    	return new InitAction(msg.getPlayers(),tm);
    }
	
    @Override
    public Action visit(MainAction1Message msg) {
    	return new MainAction1(msg.getWhichCouncillor(),msg.getWhichCouncil(),tm);
    }
    
    public Action visit(MainAction2Message msg){
    	return new MainAction2(msg.getWhichPermitCard(),msg.getWhichCouncilRegion() , tm);
    	
    }

    @Override
	public Action visit(MainAction3Message msg) {
		return new MainAction3(msg.getWhichPrivatePermitCard(),msg.getWhichCharacter(), tm);
	}

	@Override
	public Action visit(MainAction4Message msg) {
		return new MainAction4(msg.getWhichCity(), tm);
	}	
	
	@Override
	public Action visit(QuickAction1Message msg) {
		return new QuickAction1(tm);
	}

	@Override
	public Action visit(QuickAction2Message msg) {
		return new QuickAction2(msg.getWhichRegion(), tm);
	}

	@Override
	public Action visit(QuickAction3Message msg) {
		return new QuickAction3(msg.getWhichRegion(),msg.getWhichCouncillor(), tm);
	}

	@Override
	public Action visit(QuickAction4Message msg) {
		return new QuickAction4(tm);
	}

	
	
	@Override
	public Action visit(BeginMessage msg) {
		return new BeginAction(msg.getChoiceAction(), tm);
	}

	@Override
	public Action visit(NextTurnMessage msg) {
		return new NextTurnAction(tm);
	}

	@Override
	public Action visit(EndActionMessage msg) {
		return new EndAction(tm);
	}


	public Action visit(BuyMessage msg) {
		return new BuyAction(tm);
	}

	public Action visit(SellMessage msg) {
		return new SellAction(tm);
	}


}