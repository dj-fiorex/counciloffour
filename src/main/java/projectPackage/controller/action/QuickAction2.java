package projectPackage.controller.action;

import projectPackage.controller.exceptions.NotEnoughAssistants;
import projectPackage.model.turn.TurnMachine;
/**
 * Created by Med on 26/05/2016.
 */
public class QuickAction2 extends Action {

    public QuickAction2(int whichRegion, TurnMachine tm) {
        super(tm);
    	this.whichRegion = whichRegion;
    }

    private int whichRegion;

    public int getWhichRegion() {
        return whichRegion;
    }

    public void setWhichRegion(int whichRegion) {
        this.whichRegion = whichRegion;
    }

    @Override
    public void execute() throws CloneNotSupportedException {
    	if(!enoughAssistant()) throw new NotEnoughAssistants();
    	tm.start(this);
    }
    
    private boolean enoughAssistant(){
		 return tm.getGameBoard().getCurrPlayer().getNumAssistant() < 1 ? false : true;
	}
}
