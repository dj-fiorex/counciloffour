package projectPackage.controller.action;

import projectPackage.controller.exceptions.NotEnoughAssistants;
import projectPackage.model.turn.TurnMachine;

public class QuickAction4 extends Action {

    public QuickAction4(TurnMachine tm) {
    	super(tm);
    }

    @Override
    public void execute() throws CloneNotSupportedException {
    	if(!enoughAssistant()) throw new NotEnoughAssistants();
    	tm.start(this);
    }
    
    private boolean enoughAssistant(){
		 return tm.getGameBoard().getCurrPlayer().getNumAssistant() < 3 ? false : true;
	}
}
