package projectPackage.controller.action;

import java.util.LinkedList;

import projectPackage.model.Player;
import projectPackage.model.turn.TurnMachine;

public class InitAction extends Action {
	
	LinkedList<Player> players;
	// mappa

	public InitAction(LinkedList<Player> players,TurnMachine tm) {
		super(tm);
		this.players = players;
		//mappa
	}

	public LinkedList<Player> getPlayers() {
		return players;
	}

	@Override
	public void execute() throws CloneNotSupportedException {
		tm.start(this);
	}

}
