package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;

/**
 * Created by Carmelo on 19/06/2016.
 */
public class SellAction extends Action {

    public SellAction(TurnMachine tm) {
        super(tm);
    }

    @Override
    public void execute() throws CloneNotSupportedException {
        tm.start(this);
    }
}
