package projectPackage.controller.action;

import java.util.LinkedList;
import projectPackage.controller.exceptions.*;
import projectPackage.model.*;
import projectPackage.model.cards.*;
import projectPackage.model.turn.TurnMachine;

/**
 * Created by Med on 26/05/2016.
 */
public class MainAction2 extends Action {
	
	private int whichCouncilRegion;
	private int whichPermitCard;
	
	
	
	public MainAction2(int whichCouncilRegion, int whichPermitCard, TurnMachine tm) {
		super(tm);
		this.whichCouncilRegion = whichCouncilRegion;
		this.whichPermitCard = whichPermitCard;
	}



	public int getWhichCouncilRegion() {
		return whichCouncilRegion;
	}



	public int getWhichPermitCard() {
		return whichPermitCard;
	}



	@Override
	public void execute() throws CloneNotSupportedException {
		if(!enoughPoliticCards()) throw new NotEnoughPoliticCards();
		if(!thereIsPermitCard()) throw new MissingPermitCard();
		tm.start(this);
	}
	
	@SuppressWarnings("unchecked")
	private boolean enoughPoliticCards(){
		Player currPlayer = tm.getGameBoard().getCurrPlayer();
		LinkedList<Councillor> councillors = tm.getGameBoard().getRegion(this.whichCouncilRegion).getRegionCouncil().getCouncillors();
		LinkedList<PoliticCard> politicCards = (LinkedList<PoliticCard>) currPlayer.getPoliticDeck().getDeckCards().clone();//sono in aliasing? se si devo fare clone
		int cont = 0;
		for(Councillor c : councillors){
			for(PoliticCard pc : politicCards){
				if(pc.getColorCard().equals(c.getColor())){
					cont++;
					politicCards.remove(pc);
					break;
				}
			}
		}
		if(cont < 1) return false;
		if(cont == 1 && currPlayer.getMoneyPosition().getPosition() < 10) return false;
		if(cont == 2 && currPlayer.getMoneyPosition().getPosition() < 7) return false;
		if(cont == 3 && currPlayer.getMoneyPosition().getPosition() < 4) return false;
		return true;
	}
	
	private boolean thereIsPermitCard(){
		if(this.whichPermitCard == 1 && tm.getGameBoard().getRegion(this.whichCouncilRegion).getFirstPermitCard() == null) return false;
		else if(tm.getGameBoard().getRegion(this.whichCouncilRegion).getSecondPermitCard() == null) return false;
		return true;
	}
	

    
}
