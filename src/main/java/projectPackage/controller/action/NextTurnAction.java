package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;

public class NextTurnAction extends Action {
    
	
	public NextTurnAction(TurnMachine tm) {
		super(tm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute() throws CloneNotSupportedException {
		tm.start(this);
	}

}
