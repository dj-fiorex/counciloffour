package projectPackage.controller.action;

import projectPackage.controller.exceptions.NotEnoughAssistants;
import projectPackage.model.turn.TurnMachine;
/**
 * Created by Med on 26/05/2016.
 */
public class QuickAction3 extends Action {

    private int whichRegion;
    private int whichCouncillor;

    public QuickAction3(int whichRegion, int whichCouncillor, TurnMachine tm) {
     super(tm);
    	this.whichRegion = whichRegion;
        this.whichCouncillor = whichCouncillor;
    }

    public int getWhichRegion() {
        return whichRegion;
    }

    public void setWhichRegion(int whichRegion) {
        this.whichRegion = whichRegion;
    }

    public int getWhichCouncillor() {
        return whichCouncillor;
    }

    public void setWhichCouncillor(int whichCouncillor) {
        this.whichCouncillor = whichCouncillor;
    }


    @Override
    public void execute() throws CloneNotSupportedException {
    	if(!enoughAssistant()) throw new NotEnoughAssistants();
    	tm.start(this);
    }
    
    private boolean enoughAssistant(){
		 return tm.getGameBoard().getCurrPlayer().getNumAssistant() < 1 ? false : true;
	}
}
