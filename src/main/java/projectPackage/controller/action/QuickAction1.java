package projectPackage.controller.action;

import projectPackage.controller.exceptions.*;
import projectPackage.model.turn.TurnMachine;

public class QuickAction1 extends Action {

    public QuickAction1(TurnMachine tm) {
    	super(tm);
    }

    @Override
    public void execute() throws CloneNotSupportedException {
    	if(!enoughMoney()) throw new NotEnoughMoney();
    	tm.start(this);
    }
    
    private boolean enoughMoney(){
		 return tm.getGameBoard().getCurrPlayer().getMoneyPosition().getPosition() < 3 ? false : true;
	}
}
