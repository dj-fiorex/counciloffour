package projectPackage.controller.action;

import projectPackage.model.turn.TurnMachine;

/**
 * Created by Carmelo on 19/06/2016.
 */
public class BuyAction extends Action {

    public BuyAction(TurnMachine tm) {
        super(tm);
    }

    @Override
    public void execute() throws CloneNotSupportedException {
        tm.start(this);
    }
}
