package projectPackage;

import java.util.*;

import projectPackage.controller.exceptions.ActionNotAllowedException;

public abstract class Observable {
	
	private List<Observer> observers;
	
	public Observable(){
		observers=new ArrayList<Observer>();
	}
	
	public void registerObserver(Observer o){
		observers.add(o);
	}
	
	public void unregisterObserver(Observer o){
		this.observers.remove(o);
	}
	
	
	public <C> void notifyObservers(C c) throws CloneNotSupportedException{
		for(Observer o: this.observers){
			o.update(c);
		}
	}

}
